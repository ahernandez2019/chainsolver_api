FROM node:10.14.2-alpine

RUN mkdir -p /srv/api

COPY package*.json /srv/api/

WORKDIR /srv/api

RUN npm install

COPY . .

ENV RATE_LIMIT 100
ENV WEB_CONCURRENCY 1
ENV ENVIRONMENT 'STAGING'

EXPOSE 8888

## THE LIFE SAVER
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

CMD /wait && node processes.js
