FROM node:10.14.2-alpine

RUN mkdir -p /srv/api

COPY package*.json /srv/api/

ENV ENVIRONMENT 'DEVELOPMENT'

WORKDIR /srv/api

RUN npm install -g nodemon

RUN npm install

COPY . .

EXPOSE 8888

## THE LIFE SAVER
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

CMD /wait && nodemon server.js
