// Libraries
const express = require('express')

// Controllers
const handlingUnitController = require('../controllers/handlingUnitsController')

// Router
const handlingUnits = express.Router()

handlingUnits.get('/', handlingUnitController.getAll)
handlingUnits.get('/:id', handlingUnitController.getById)
handlingUnits.post('/', handlingUnitController.create)
handlingUnits.patch('/:id', handlingUnitController.update)
handlingUnits.delete('/:id', handlingUnitController.destroy)
handlingUnits.patch('/', handlingUnitController.updateMultiple)
handlingUnits.delete('/', handlingUnitController.destroyMultiple)

module.exports = handlingUnits
