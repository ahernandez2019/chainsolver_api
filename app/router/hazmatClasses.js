// Libraries
const express = require('express')

// Controllers
const hazmatClassesController = require('../controllers/hazmatClassesController')

// Router
const hazmatClasses = express.Router()

hazmatClasses.get('/', hazmatClassesController.getAll)
hazmatClasses.post('/', hazmatClassesController.create)

module.exports = hazmatClasses
