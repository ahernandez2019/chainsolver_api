// Libraries
const express = require('express')

// Controllers
const phonesController = require('../controllers/phonesController')

// Router
const phones = express.Router()

phones.get('/', phonesController.getAll)
phones.post('/', phonesController.create)

module.exports = phones
