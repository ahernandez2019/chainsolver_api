// Libraries
const express = require('express')

// Controllers
const itemsController = require('../controllers/itemsController')

// Router
const items = express.Router()

items.get('/', itemsController.getAll)
items.get('/:id', itemsController.getById)
items.post('/', itemsController.create)
items.patch('/:id', itemsController.update)
items.delete('/:id', itemsController.destroy)
items.patch('/', itemsController.updateMultiple)
items.delete('/', itemsController.destroyMultiple)

module.exports = items
