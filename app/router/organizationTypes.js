// Libraries
const express = require('express')

// Controllers
const organizationTypesController = require('../controllers/organizationTypesController')

// Router
const organizationTypes = express.Router()

organizationTypes.get('/', organizationTypesController.getAll)
organizationTypes.post('/', organizationTypesController.create)

module.exports = organizationTypes
