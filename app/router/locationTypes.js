// Libraries
const express = require('express')

// Controllers
const locationTypesController = require('../controllers/locationTypesController')

// Router
const contacts = express.Router()

contacts.get('/', locationTypesController.getAll)
contacts.post('/', locationTypesController.create)

module.exports = contacts
