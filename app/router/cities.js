// Libraries
const express = require('express')

// Controllers
const citiesController = require('../controllers/citiesController')

// Router
const cities = express.Router()

cities.get('/', citiesController.getAll)
cities.post('/', citiesController.create)

module.exports = cities
