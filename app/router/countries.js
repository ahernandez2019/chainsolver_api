// Libraries
const express = require('express')

// Controllers
const countriesController = require('../controllers/countriesController')

// Router
const countries = express.Router()

countries.get('/', countriesController.getAll)
countries.post('/', countriesController.create)

module.exports = countries
