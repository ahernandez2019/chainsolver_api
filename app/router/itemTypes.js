// Libraries
const express = require('express')

// Controllers
const itemTypesController = require('../controllers/itemTypesController')

// Router
const itemTypes = express.Router()

itemTypes.get('/', itemTypesController.getAll)
itemTypes.post('/', itemTypesController.create)

module.exports = itemTypes
