// Libraries
const express = require('express')

// Controllers
const packagesController = require('../controllers/packagesController')

// Router
const packages = express.Router()

packages.get('/', packagesController.getAll)
packages.get('/:id', packagesController.getById)
packages.post('/', packagesController.create)
packages.patch('/:id', packagesController.update)
packages.delete('/:id', packagesController.destroy)
packages.patch('/', packagesController.updateMultiple)
packages.delete('/', packagesController.destroyMultiple)

module.exports = packages
