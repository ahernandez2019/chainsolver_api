// Libraries
const express = require('express')

// Controllers
const statesController = require('../controllers/statesController')

// Router
const states = express.Router()

states.get('/', statesController.getAll)
states.post('/', statesController.create)

module.exports = states
