// Libraries
const express = require('express')

// Controllers
const customFieldsController = require('../controllers/customFieldsController')

// Router
const customFields = express.Router()

customFields.get('/', customFieldsController.getAll)
customFields.post('/', customFieldsController.create)

module.exports = customFields
