// Libraries
const express = require('express')

// Controllers
const gearTypesController = require('../controllers/gearTypesController')

// Router
const gearTypes = express.Router()

gearTypes.get('/', gearTypesController.getAll)
gearTypes.post('/', gearTypesController.create)

module.exports = gearTypes
