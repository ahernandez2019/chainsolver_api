// Libraries
const express = require('express')

// Controllers
const adminsController = require('../controllers/adminsController')

// Router
const admins = express.Router()

admins.get('/', adminsController.getAll)

module.exports = admins
