// Libraries
const express = require('express')

// Controllers
const handlingUnitTypesController = require('../controllers/handlingUnitTypesController')

// Router
const handlingUnitTypes = express.Router()

handlingUnitTypes.get('/', handlingUnitTypesController.getAll)
handlingUnitTypes.post('/', handlingUnitTypesController.create)

module.exports = handlingUnitTypes
