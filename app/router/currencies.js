// Libraries
const express = require('express')

// Controllers
const currenciesController = require('../controllers/currenciesController')

// Router
const currencies = express.Router()

currencies.get('/', currenciesController.getAll)
currencies.post('/', currenciesController.create)

module.exports = currencies
