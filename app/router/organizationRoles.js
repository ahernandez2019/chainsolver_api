// Libraries
const express = require('express')

// Controllers
const organizationRolesController = require('../controllers/organizationRolesController')

// Router
const organizationRoles = express.Router()

organizationRoles.get('/', organizationRolesController.getAll)
organizationRoles.post('/', organizationRolesController.create)

module.exports = organizationRoles
