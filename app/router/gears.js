// Libraries
const express = require('express')

// Controllers
const gearsController = require('../controllers/gearsController')

// Router
const gearRouter = express.Router()

gearRouter.get('/', gearsController.getAll)
gearRouter.get('/:id', gearsController.getById)
gearRouter.post('/', gearsController.create)
gearRouter.patch('/:id', gearsController.update)
gearRouter.delete('/:id', gearsController.destroy)

module.exports = gearRouter
