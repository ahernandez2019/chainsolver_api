// Libraries
const express = require('express')

// Controllers
const itemCategoriesController = require('../controllers/itemCategoriesController')

// Router
const itemCategories = express.Router()

itemCategories.get('/', itemCategoriesController.getAll)
itemCategories.get('/:id', itemCategoriesController.getById)

module.exports = itemCategories
