// Libraries
const express = require('express')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', { session: false })
const requireLogin = passport.authenticate('local', { session: false })
const checkToken = require('../middlewares/accessControl')

// Controllers
const authenticationController = require('../controllers/authenticationController')
const usersController = require('../controllers/usersController')

// Router
const accounts = express.Router()

accounts.post('/sign-up', authenticationController.register)
accounts.get('/:id', checkToken, requireAuth, usersController.getAccount)
accounts.patch('/:id', requireAuth, usersController.updateAccount)
accounts.delete('/:id', requireAuth, usersController.deleteAccount)
accounts.post('/login', requireLogin, authenticationController.login)
accounts.post('/logout', requireAuth, authenticationController.logout)
accounts.post('/verify-email/:token', authenticationController.verifyEmail)
accounts.post('/reset-password/:token', authenticationController.resetPassword)

module.exports = accounts
