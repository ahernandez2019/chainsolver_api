// Libraries
const express = require('express')

// Controllers
const packingTypesController = require('../controllers/packingTypesController')

// Router
const packingTypes = express.Router()

packingTypes.get('/', packingTypesController.getAll)
packingTypes.post('/', packingTypesController.create)

module.exports = packingTypes
