// Libraries
const express = require('express')

// Controllers
const lengthUnitsController = require('../controllers/lengthUnitsController')

// Router
const lengthUnits = express.Router()

lengthUnits.get('/', lengthUnitsController.getAll)

module.exports = lengthUnits
