// Libraries
const express = require('express')

// Controllers
const contactsController = require('../controllers/contactsController')

// Router
const contacts = express.Router()

contacts.get('/', contactsController.getAll)
contacts.get('/:id', contactsController.getById)
contacts.post('/', contactsController.create)
contacts.patch('/:id', contactsController.update)
contacts.delete('/:id', contactsController.destroy)

module.exports = contacts
