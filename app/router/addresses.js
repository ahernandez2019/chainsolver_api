// Libraries
const express = require('express')

// Controllers
const addressesController = require('../controllers/addressesController')

// Router
const users = express.Router()

users.get('/', addressesController.getAll)
users.post('/', addressesController.create)

module.exports = users
