// Libraries
const express = require('express')

// Controllers
const organizationMembersController = require('../controllers/organizationMembersController')

// Router
const organizationMembers = express.Router()

organizationMembers.get('/', organizationMembersController.getAll)
organizationMembers.post('/', organizationMembersController.create)

module.exports = organizationMembers
