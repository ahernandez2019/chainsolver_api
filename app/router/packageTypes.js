// Libraries
const express = require('express')

// Controllers
const packageTypesController = require('../controllers/packageTypesController')

// Router
const packageTypes = express.Router()

packageTypes.get('/', packageTypesController.getAll)
packageTypes.post('/', packageTypesController.create)

module.exports = packageTypes
