// Libraries
const express = require('express')

// Controllers
const weightUnitsController = require('../controllers/weightUnitsController')

// Router
const weightUnits = express.Router()

weightUnits.get('/', weightUnitsController.getAll)

module.exports = weightUnits
