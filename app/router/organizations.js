// Libraries
const express = require('express')

// Controllers
const organizationsController = require('../controllers/organizationsController')

// Router
const organizations = express.Router()

organizations.get('/', organizationsController.getAll)
organizations.get('/:id', organizationsController.getById)
organizations.post('/', organizationsController.create)
organizations.patch('/:id', organizationsController.update)
organizations.delete('/:id', organizationsController.destroy)

module.exports = organizations
