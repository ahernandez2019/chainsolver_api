// Libraries
const express = require('express')

// Controllers
const locationsController = require('../controllers/locationsController')

// Router
const locations = express.Router()

locations.get('/', locationsController.getAll)
locations.get('/:id', locationsController.getById)
locations.post('/', locationsController.create)
locations.patch('/:id', locationsController.update)
locations.delete('/:id', locationsController.destroy)

module.exports = locations
