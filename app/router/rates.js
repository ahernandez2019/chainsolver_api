// Libraries
const express = require('express')

// Controllers
const ratesController = require('../controllers/ratesController')

// Router
const rates = express.Router()
//  Create
rates.post('/', ratesController.create)
//  Update
rates.patch('/:id', ratesController.update)
//  Read
rates.get('/:id', ratesController.getOne)
rates.get('/', ratesController.getAll)
//  Delete
rates.delete('/:id', ratesController.deleteOne)
rates.delete('/', ratesController.deleteMultiple)

module.exports = rates
