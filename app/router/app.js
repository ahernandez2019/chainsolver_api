// Libraries
const express = require('express')

// Controllers
const appController = require('../controllers/appController')

// Router
const app = express.Router()

app.get('/:action', appController.executeAction)

module.exports = app
