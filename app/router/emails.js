// Libraries
const express = require('express')

// Controllers
const emailsController = require('../controllers/emailsController')

// Router
const emails = express.Router()
emails.post('/welcome/:id', emailsController.welcomeMessage)
emails.post('/verify', emailsController.verificationEmail)
emails.post('/reset-password', emailsController.resetPasswordEmail)
module.exports = emails
