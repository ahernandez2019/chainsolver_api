const express = require('express')
const router = express.Router({ strict: true })

if (['development', 'staging'].includes(process.env.ENVIRONMENT)) {
  router.use('/app', require('./app'))
}

// router.use('/rates', require('./rates'))

module.exports = router
