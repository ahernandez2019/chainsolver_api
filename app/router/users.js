// Libraries
const express = require('express')

// Controllers
const usersController = require('../controllers/usersController')

// Router
const users = express.Router()

users.get('/', usersController.getAll)
users.post('/', usersController.create)

users.get('/:id', usersController.getById)

module.exports = users
