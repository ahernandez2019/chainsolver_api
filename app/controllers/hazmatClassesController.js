const Models = require('../models')

async function getAll (req, res) {
  try {
    const results = await Models.HazmatClass.findAll()
    res.status(200).send({ data: results })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const result = await Models.HazmatClass.build(req.body).save()
    res.status(200).send({
      data: result,
      message: 'Hazmat Class created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
