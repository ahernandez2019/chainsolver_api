const Models = require('../models')
const _ = require('lodash')

async function getAll (req, res) {
  const limit = _.get(req, 'query.limit', 25)
  const offset = _.get(req, 'query.offset', 0)

  try {
    const states = await Models.State.findAll({ limit, offset })
    res.status(200).send({ data: states })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const state = await Models.State.build(req.body).save()

    res.status(200).send({
      data: state,
      message: 'State created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
