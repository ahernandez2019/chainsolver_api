const Models = require('../models')
const _ = require('lodash')

async function getAll (req, res) {
  try {
    const limit = _.get(req, 'query.limit', 25)
    const offset = _.get(req, 'query.offset', 0)
    const cities = await Models.City.findAll({ limit, offset })
    res.status(200).send({ data: cities })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const city = await Models.City.build(req.body).save()
    res.status(200).send({
      data: city,
      message: 'City created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
