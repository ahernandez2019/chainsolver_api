const Models = require('../models')
const _ = require('lodash')

const getAll = async (req, res) => {
  const limit = _.get(req, 'query.limit', 25)
  const offset = _.get(req, 'query.offset', 0)
  const findParams = {
    limit,
    offset
  }
  try {
    const ItemCategorys = await Models.ItemCategory.findAndCountAll(findParams)
    const { count, rows } = ItemCategorys
    res.status(200).send({
      count,
      rows
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

const getById = async (req, res) => {
  const id = _.parseInt(req.params.id)
  const ItemCategory = await Models.ItemCategory.findByPk(id)
  if (_.isEmpty(ItemCategory)) {
    return res.status(500).send({
      message: 'ItemCategory not exists'
    })
  }
  try {
    res.status(200).send({
      data: ItemCategory,
      message: 'ItemCategory found'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

module.exports = {
  getAll,
  getById
}
