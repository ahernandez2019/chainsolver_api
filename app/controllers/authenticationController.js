const jwt = require('jsonwebtoken')
const Models = require('../models')
const client = require('../cache')
async function generateToken (user) {
  try {
    return await jwt.sign(user, process.env.JWT_SECRET, {
      expiresIn: 10000 // in seconds
    })
  } catch (e) {
    return e
  }
}
const register = async function (req, res, next) {
  try {
    let { email, name, password, organization, updatedAt,
      createdAt } = req.body
    if (!email) {
      return res.status(422).send({ error: 'You must enter an email address.' })
    }
    if (!name) {
      return res.status(422).send({ error: 'You must enter your full name.' })
    }
    if (!password) {
      return res.status(422).send({ error: 'You must enter a password.' })
    }
    if (!organization) {
      return res.status(422).send({ error: 'You must be part of an organization.' })
    }
    let userSaved
    let user = await Models.User.findOne({ where: { email } })
    if (user) {
      return res.status(422).send({ error: 'That email address is already in use.' })
    } else {
      let organizationSaved
      await Models.sequelize.transaction(async transaction => {
        organizationSaved = await Models.Organization.create({ name: organization.name }, { transaction })
        const OrganizationId = organizationSaved.id
        const payload = {
          name,
          email,
          password,
          OrganizationId,
          updatedAt,
          createdAt,
          temporaryToken: await jwt.sign({ name, email, password, OrganizationId },
            process.env.JWT_SECRET, { expiresIn: '24h' })
        }
        userSaved = await Models.User.create(payload, { transaction })
      })
      return res.status(201).json({
        token: 'Authorization  ' + await generateToken({ name, email, password }),
        user: userSaved,
        organization: organizationSaved
      })
    }
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
const verifyEmail = async function (req, res, next) {
  try {
    let token = req.params.token
    if (!token) {
      return res.status(422).send({ error: 'no token provided.' })
    }
    let user = await Models.User.findOne({ where: { temporaryToken: token } })
    if (user) {
      user.temporaryToken = false
      user.isVerified = true
      const userSaved = await user.save()
      return res.status(201).json({
        message: `congratulations ${userSaved.name}, your account is now activated!`
      })
    } else if (!user) {
      const message = 'invalid token'
      res.status(400).send({ message })
    }
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
const login = async function (req, res, next) {
  try {
    const {
      name,
      email,
      id
    } = req.user
    return res.status(200).json({
      token: 'Authorization ' + await generateToken({
        name,
        email,
        id
      }),
      user: req.user
    })
  } catch (error) {
    return res.status(400).json({
      error
    })
  }
}
const resetPassword = async function (req, res, next) {
  try {
    let token = req.params.token
    let password = req.body.password
    if (!token) {
      return res.status(422).send({ error: 'no token provided.' })
    }
    if (!password) {
      return res.status(422).send({ error: 'no password.' })
    }
    let user = await Models.User.findOne({ where: { resetToken: token } })
    if (user) {
      user.resetToken = false
      user.password = password
      const userSaved = await user.save()
      return res.status(201).json({
        user: userSaved,
        message: 'password changed successfuly'
      })
    } else if (!user) {
      const message = 'invalid token'
      res.status(400).send({ message })
    }
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
const logout = async function (req, res, next) {
  try {
    const token = req.headers.authorization.split(' ')[1]

    await client.set(token, 'true', 'NX', 'EX', '10000')
    res.status(200).json({
      message: 'log out succesful'
    })
  } catch (e) {
    res.status(500).json({
      message: e
    })
  }
}
module.exports = {
  login,
  logout,
  register,
  verifyEmail,
  resetPassword
}
