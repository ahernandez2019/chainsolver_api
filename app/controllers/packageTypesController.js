const Models = require('../models')

async function getAll (req, res) {
  try {
    const packageTypes = await Models.PackageType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: packageTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const packageType = await Models.PackageType.build(req.body).save()
    res.status(200).send({
      data: packageType,
      message: 'packageType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
