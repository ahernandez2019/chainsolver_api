
const rebuildDB = async mock => {
  try {
    const db = require('../db')
    await db.sync(true, mock)
  } catch (error) {
    throw new Error(error)
  }
  return 'database rebuild success'
}

const executeAction = async (req, res) => {
  try {
    const { action } = req.params
    let response
    const { mock } = req.query
    if (action === 'rebuildBd') {
      response = await rebuildDB(mock)
    }
    return res.status(200).json(response)
  } catch (error) {
    return res.status(500).json(error)
  }
}

module.exports = {
  executeAction
}
