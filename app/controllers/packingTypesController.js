const Models = require('../models')

async function getAll (req, res) {
  try {
    const PackingTypes = await Models.PackingType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: PackingTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const PackingType = await Models.PackingType.build(req.body).save()
    res.status(200).send({
      data: PackingType,
      message: 'PackingType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
