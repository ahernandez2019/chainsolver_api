const Models = require('../models')

async function getAll (req, res) {
  try {
    const organizationMembers = await Models.OrganizationMember.findAll()
    res.status(200).send({ data: organizationMembers })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const organizationMember = await Models.OrganizationMember.build(req.body).save()
    res.status(200).send({
      data: organizationMember,
      message: 'OrganizationMember created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
