const Models = require('../models')

async function getAll (req, res) {
  try {
    const addresses = await Models.Address.findAll()
    res.status(200).send({ data: addresses })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const address = await Models.Address.build(req.body).save()

    res.status(200).send({
      data: address,
      message: 'Address created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
