const sgMail = require('@sendgrid/mail')
const jwt = require('jsonwebtoken')
const models = require('../models')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const emails = {
  info: 'info@innovativefreightalgorithms.com'
}

const sendMessage = async function (from, to, subject, text, html, templateId, data) {
  try {
    let emailSent
    await sgMail.send({
      from,
      to,
      subject,
      text,
      html,
      templateId,
      dynamic_template_data: data
    }, (err, success) => {
      if (err) {
        throw err
      }
      emailSent = success
    })
    return {
      emailSent
    }
  } catch (error) {
    throw error
  }
}
const welcomeMessage = async function (req, res, next) {
  try {
    const user = await models.User.findByPk(req.params.id)
    if (user) {
      const from = emails.info
      const to = user.email
      const text = `Hello ${user.name},thank you for registering at Innovative Freight Algorithms Please click on the following link to complete your activation: ${process.env.SITE_URL}/account/verify-email/${user.temporaryToken}`
      const subject = `Hello ${user.name}, Welcome to Primus!`
      const html = `Some data`
      const templateId = 'd-eb55eaf086234e30809ec401b1d2ea33'
      const data = {
        user_name: user.name,
        subject: `Hello ${user.name}, Welcome to Primus!`,
        link: `${process.env.SITE_URL}/account/verify-email/${user.temporaryToken}`
      }
      const emailSent = await sendMessage(from, to, subject, text, html, templateId, data)
      let message
      if (emailSent.error) {
        throw new Error(emailSent.e)
      }
      message = 'Message sent succesfully'
      res.status(200).send({ status: emailSent, message })
    } else {
      const message = 'This email doesnt match to any user'
      res.status(400).send({ message })
    }
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const verificationEmail = async function (req, res, next) {
  try {
    const user = await models.User.findOne({
      where: {
        email: req.body.email
      }
    })
    if (user) {
      const { name, email, password, OrganizationId } = user
      user.temporaryToken = await jwt.sign({ name, email, password, OrganizationId },
        process.env.JWT_SECRET, { expiresIn: '24h' })
      const userSaved = await user.save()
      const from = emails.info
      const to = email
      const text = `Hello ${name},You recently requested a new account activation link. Please click on the following link to complete your activation: ${process.env.SITE_URL}/account/verify-email/${userSaved.temporarytoken}`
      const subject = `Primus Account Activation Link Request`
      const html = `some data`
      const templateId = 'd-25c9ba43099f476e8ce14dce5b976460'
      const data = {
        user_name: name,
        subject: `Primus Account Activation Link Request`,
        link: `${process.env.SITE_URL}/account/verify-email/${userSaved.temporaryToken}`
      }
      const emailSent = await sendMessage(from, to, subject, text, html, templateId, data)
      if (emailSent.error) {
        throw new Error(emailSent.e)
      }
      const message = 'Please check your e-mail for activation link.'
      return res.status(200).send({ status: emailSent.emailSent.statusCode, message })
    } else if (!user) {
      return res.status(400).json({
        message: 'account not found'
      })
    }
  } catch (error) {
    return res.status(400).json({
      message: 'Error found',
      error
    })
  }
}

const resetPasswordEmail = async function (req, res, next) {
  try {
    const user = await models.User.findOne({
      where: {
        email: req.body.email
      }
    })
    if (user) {
      const { name, email, password, OrganizationId } = user
      user.resetToken = await jwt.sign({ name, email, password, OrganizationId },
        process.env.JWT_SECRET, { expiresIn: '1h' })
      const userSaved = await user.save()
      const from = emails.info
      const to = email
      const subject = 'Primus reset password Link Request'
      const text = `
        Hello ${name}You recently requested a password reset link. Please click on the following link to complete your activation: ${process.env.SITE_URL}/accounts/reset-password/${userSaved.resetToken}`
      const message = 'Email Sent, Please check your e-mail for reset link.'
      const html = `some data`
      const templateId = 'd-efebc62b0f06469b91cc171b2608de93'
      const data = {
        user_name: name,
        subject: 'Primus reset password Link Request',
        link: `${process.env.SITE_URL}/accounts/reset-password/${userSaved.resetToken}`
      }
      const emailSent = await sendMessage(from, to, subject, text, html, templateId, data)
      if (emailSent.error) {
        throw new Error(emailSent.e)
      }
      return res.status(200).send({ status: emailSent.emailSent.statusCode, message })
    } else if (!user) {
      return res.status(400).json({
        message: 'user not found'
      })
    }
  } catch (error) {
    return res.status(400).json({
      message: 'Error found',
      error
    })
  }
}

module.exports = {
  welcomeMessage,
  verificationEmail,
  resetPasswordEmail
}
