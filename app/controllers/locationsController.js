const Models = require('../models')
const { includeGet, includeUpdate } = require('./includes/locations')
const { setCity } = require('./utils/cities')
const _ = require('lodash')

const getAll = async (req, res) => {
  const findParams = {
    ...includeGet(Models),
    limit: _.get(req, 'query.limit', 25),
    offset: _.get(req, 'query.offset', 0),
    where: {
      isHq: false
    }
  }

  if (req.query.organizationId !== undefined) {
    _.set(findParams, 'where.OrganizationId', req.query.organizationId)
  }

  try {
    const locations = await Models.Location.findAll(findParams)
    res.status(200).send({ data: locations })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const getById = async (req, res) => {
  const { id } = req.params
  try {
    const findParams = {
      ...includeGet(Models),
      where: {
        isHq: false
      }
    }

    const location = await Models.Location.findByPk(id, findParams)

    res.status(200).send({ data: location })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const create = async (req, res) => {
  try {
    const {
      location: {
        OrganizationId,
        LocationTypeId,
        name,
        address,
        lat,
        lng,
        city,
        state,
        country,
        zip,
        loadingDock,
        forklift,
        helpLoading,
        hoursOperation,
        needsAppointment,
        phone,
        notes,
        contacts
      }
    } = req.body
    let locationSaved

    await Models.sequelize.transaction(async transaction => {
      const locationCity = await setCity(Models, city, country, state, transaction)
      let PhoneId
      if (phone && phone.id) {
        PhoneId = phone.id
      } else if (phone && phone.number) {
        const Phone = await Models.Phone.findOne({
          where: {
            number: phone.number
          }
        }, { raw: true })
        if (Phone) {
          PhoneId = Phone.id
        } else if (!Phone) {
          const phoneBody = {
            number: phone.number,
            createdBy: 1,
            updatedBy: 1,
            createdAt: '2019-06-15T07:44:12.154Z',
            updatedAt: '2019-06-15T07:23:25.913Z'
          }
          const phoneSaved = await Models.Phone.create(phoneBody, { transaction })
          PhoneId = phoneSaved.id
        }
      }
      const locationBody = {
        OrganizationId,
        LocationTypeId,
        PhoneId,
        isHq: false,
        name,
        loadingDock: loadingDock === 'true',
        forklift: forklift === 'true',
        helpLoading: helpLoading === 'true',
        needsAppointment: needsAppointment === 'true',
        operationHours: hoursOperation,
        notes: notes,
        contacts: contacts
      }
      const addressBody = {
        address,
        zipCode: zip,
        coordinates: lat && lng && `${lat}|${lng}`,
        CityId: locationCity
      }

      const addrsaved = await Models.Address.create(addressBody, { transaction })
      locationBody.AddressId = addrsaved.id
      locationSaved = await Models.Location.create(locationBody, { transaction })

      // if we have contacts, create them
      if (contacts) {
        const contactsToSave = contacts.map(contact => ({
          ...contact,
          LocationId: locationSaved.id
        }))
        await Models.Contact.bulkCreate(contactsToSave, { transaction })
      }
    })

    locationSaved = await Models.Location.findByPk(locationSaved.id, includeGet(Models))

    res.status(200).send({
      data: locationSaved,
      message: 'Location created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const update = async (req, res) => {
  try {
    const { id } = req.params
    const {
      location: {
        OrganizationId,
        LocationTypeId,
        name,
        address,
        lat,
        lng,
        city,
        state,
        country,
        zip,
        loadingDock,
        forklift,
        helpLoading,
        hoursOperation,
        needsAppointment,
        phone,
        notes
      }
    } = req.body

    let locationSaved = await Models.Location.findByPk(id, {
      ...includeUpdate(Models),
      where: {
        isHq: false
      }
    })

    if (_.isEmpty(locationSaved)) {
      return res.status(500).send({
        message: 'Location not exists'
      })
    }
    await Models.sequelize.transaction(async transaction => {
      const locationCity = await setCity(Models, city, country, state, transaction)
      let PhoneId
      if (phone && phone.id) {
        PhoneId = phone.id
      } else if (phone && phone.number) {
        const Phone = await Models.Phone.findOne({
          where: {
            number: phone.number
          }
        }, { raw: true })
        if (Phone) {
          PhoneId = Phone.id
        } else if (!Phone) {
          const phoneBody = {
            number: phone.number,
            createdBy: 1,
            updatedBy: 1,
            createdAt: '2019-06-15T07:44:12.154Z',
            updatedAt: '2019-06-15T07:23:25.913Z'
          }
          const phoneSaved = await Models.Phone.create(phoneBody, { transaction })
          PhoneId = phoneSaved.id
        }
      }
      const locationBody = {
        OrganizationId,
        LocationTypeId,
        PhoneId,
        name,
        isHq: false,
        loadingDock: loadingDock === 'true',
        forklift: forklift === 'true',
        helpLoading: helpLoading === 'true',
        needsAppointment: needsAppointment === 'true',
        operationHours: hoursOperation,
        notes: notes
      }

      await locationSaved.update({ ...locationBody }, { transaction })
      await locationSaved.Address.update({
        address,
        zipCode: zip,
        coordinates: lat && lng && `${lat}|${lng}`,
        CityId: locationCity
      }, { transaction })
    })

    locationSaved = await Models.Location.findByPk(locationSaved.id, includeGet(Models))

    return res.status(200).send({
      data: locationSaved,
      message: 'Location updated successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    return res.status(500).send({ message })
  }
}

const reassignContacts = async (res, reassign, locationSaved, id) => {
  if (!isNaN(reassign)) {
    if (reassign === id || reassign === 0) {
      throw new Error('ID conflicts')
    }
  } else {
    let locationHQ

    try {
      locationHQ = await Models.Location.findOne({
        where: {
          isHq: true,
          OrganizationId: locationSaved.OrganizationId
        },
        attributes: ['id']
      })
    } catch (error) {
      console.log('error', error)
      throw new Error('Eror finding HQ')
    }

    if (_.isEmpty(locationHQ)) {
      throw new Error('Cant find HQ')
    }

    reassign = _.parseInt(locationHQ.id)
  }

  try {
    await Models.sequelize.query('UPDATE "Contacts" set "LocationId"=:locationId where "LocationId"=:id', {
      replacements: {
        id,
        locationId: reassign
      },
      type: Models.sequelize.QueryTypes.UPDATE
    })
  } catch (error) {
    console.log('error', error)
    throw new Error('Contacts cant be updated')
  }
}

const destroy = async (req, res) => {
  const id = _.parseInt(req.params.id) || 0

  let { reassign } = req.query

  const findParams = {
    ...includeUpdate(Models),
    where: {
      id
    }
  }

  const locationSaved = await Models.Location.findOne(findParams)

  if (_.isEmpty(locationSaved)) {
    return res.status(500).send({
      message: 'Location not exists'
    })
  }
  if (locationSaved.isHq === true) {
    return res.status(500).send({
      message: 'Hq location cannot be deleted'
    })
  }
  /**
   * If the param `reassign` is defined, the user wants:
   * - relocate the users in another location
   * - unassign the users (assign to HQ location)
   */
  try {
    const addressId = locationSaved.AddressId
    await Models.sequelize.transaction(async transaction => {
      if (reassign !== undefined) {
        await reassignContacts(res, _.parseInt(reassign), locationSaved, id)
      }
      await locationSaved.destroy()
      await Models.Address.destroy({
        where: {
          id: addressId
        }
      })
    })
    return res.status(200).send({
      message: 'Location deleted successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    return res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy
}
