const Models = require('../models')

async function getAll (req, res) {
  try {
    const users = await Models.User.findAll()
    res.status(200).send({ data: users })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
async function getAccount (req, res) {
  try {
    const user = await Models.User.findByPk(req.user.id)
    res.status(200).send({ data: user })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const user = await Models.User.build(req.body).save()

    res.status(200).send({
      data: user,
      message: 'Created user successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function getById (req, res) {
  try {
    const user = await Models.User.findByPk(req.params.id)
    res.status(200).send({
      data: user,
      message: 'User found'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
async function updateAccount (req, res) {
  try {
    const { Account: {
      email, name, password, organization
    } } = req.body
    const user = await Models.User.findByPk(req.params.id)
    const userSaved = await user.update({
      email, name, password, organization
    })
    res.status(200).send({
      data: userSaved,
      message: 'User Updated'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
async function deleteAccount (req, res) {
  try {
    const user = await Models.User.destroy({
      where: {
        id: req.params.id
      }
    })

    res.status(200).send({
      user,
      message: 'Account succesfully deleted'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  deleteAccount,
  updateAccount,
  getAll,
  create,
  getById,
  getAccount
}
