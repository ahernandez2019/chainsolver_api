const Models = require('../models')
const _ = require('lodash')
const {
  includeGet
} = require('./includes/packages')

const getItemsWithoutData = items => items.filter(itemObject =>
  _.isEmpty(itemObject) || _.isEmpty(itemObject, true))

const getItemsToBeLinked = items => items.filter(itemObject => itemObject.id)

const getItemsToBeCreated = items => items.filter(itemObject => !itemObject.id)

const getItemsWithInvalidData = items => (
  items.filter(itemObject => {
    const {
      id,
      quantity,
      ...rest
    } = itemObject
    return _.isEmpty(itemObject) ||
      (id && !quantity && !_.isEmpty(rest)) ||
      (!id && quantity && _.isEmpty(rest)) ||
      (id && quantity && !_.isEmpty(rest))
  })
)

const getAll = async (req, res) => {
  let limit = _.get(req, 'query.limit', 25)
  let offset = _.get(req, 'query.offset', 0)

  const findParams = {
    ...includeGet(Models),
    limit,
    offset
  }
  try {
    const where = {}
    if (req.query.OrganizationId) {
      where.OrganizationId = req.query.OrganizationId
    }
    if (req.query.LocationId) {
      where.manufacturerLocationId = req.query.LocationId
    }
    const itemsIds = await Models.Item.findAll({
      where
    }).map(i => i.id)
    const packages = await Models.PackageItems.findAll({
      where: {
        ItemId: {
          in: itemsIds
        }
      }
    })
    const packagesIds = _.uniqBy(packages, 'PackageId').map(p => p.PackageId)
    const packagesFound = await Models.Package.findAndCountAll(findParams, {
      where: {
        id: {
          in: packagesIds
        }
      }
    })
    const {
      count,
      rows
    } = packagesFound

    res.status(200).send({
      count,
      rows
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}

const create = async (req, res) => {
  try {
    const {
      package: {
        items,
        ...packageData
      }
    } = req.body

    if (items) {
      const itemsWithInvalidData = getItemsWithInvalidData(items)
      const itemsWithoutData = getItemsWithoutData(items)
      if (itemsWithoutData.length) {
        throw new Error(`you can't create a item with empty data`)
      } else if (itemsWithInvalidData.length) {
        throw new Error(`Invalid item data sent. Not allowed to send (id + quantity + item's data)  
          or (id + item's data) `)
      }
    } else if (!items || !items[0]) {
      return res.status(500).json({
        message: 'cannot create packages without items'
      })
    }
    let packageSaved
    await Models.sequelize.transaction(async transaction => {
      packageSaved = await Models.Package.create(packageData, {
        ...includeGet(Models),
        transaction
      })

      if (items) {
        const itemsToCreate = getItemsToBeCreated(items)
        const itemsToLink = getItemsToBeLinked(items).map(itemObject => ({
          id: itemObject.id,
          quantity: (itemObject.quantity || 1)
        }))
        const itemsResult = await Models.Item.bulkCreate(itemsToCreate, {
          transaction,
          returning: true
        })

        let itemsSavedWithQuantity = itemsResult.map((itemObj, i) => ({
          id: itemObj.id,
          quantity: (itemsToCreate[i].quantity || 1)
        }))

        itemsSavedWithQuantity = [...itemsSavedWithQuantity, ...itemsToLink]

        const packageItemsRecords = itemsSavedWithQuantity.map((itemObject, i) => ({
          PackageId: packageSaved.id,
          ItemId: itemObject.id,
          quantity: itemObject.quantity
        }))
        await Models.PackageItems.bulkCreate(packageItemsRecords, {
          transaction
        })
      }
    })

    const packageFound = await Models.Package.findByPk(packageSaved.id, includeGet(Models))
    res.status(200).json({
      data: packageFound,
      message: 'Package created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}

const getById = async (req, res) => {
  const id = _.parseInt(req.params.id)
  const packageFound = await Models.Package.findByPk(id, includeGet(Models))
  if (_.isEmpty(packageFound)) {
    return res.status(200).send({
      message: 'Package not exists'
    })
  }
  try {
    res.status(200).send({
      data: packageFound
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}

const update = async (req, res) => {
  try {
    const { package: { items, ...packageData } } = req.body

    if (items) {
      const itemsWithoutData = getItemsWithoutData(items)
      if (itemsWithoutData.length) {
        throw new Error(`you can't create a item with empty data`)
      }
    }

    let packageFound
    await Models.sequelize.transaction(async transaction => {
      const result = await Models.Package.update(packageData, { transaction, returning: true, where: { id: req.params.id } })
      const [rowsAffected, packageResult] = result
      const packageSaved = packageResult[0]
      if (!rowsAffected) {
        return res.status(200).json({ status: 200, data: packageResult })
      }

      if (items) {
        const itemsToCreate = getItemsToBeCreated(items)
        const itemsToLink = getItemsToBeLinked(items)
        await packageSaved.setItems([], { transaction })

        const itemsResult = await Models.Item.bulkCreate(itemsToCreate, { transaction, returning: true })

        const packageItemsRecords = itemsResult.map((itemObject, i) => (
          { 'PackageId': packageSaved.id, 'ItemId': itemObject.id, 'quantity': itemsToCreate[i].quantity || 1 }
        ))

        const packageItemsToLinkRecords = itemsToLink.map((itemObject) => (
          { 'PackageId': packageSaved.id, 'ItemId': itemObject.id, 'quantity': itemObject.quantity || 1 }
        ))
        await Models.PackageItems.bulkCreate([...packageItemsRecords, ...packageItemsToLinkRecords], { transaction })
      }
    })

    packageFound = await Models.Package.findByPk(req.params.id, includeGet(Models))
    res.status(200).json({
      data: packageFound,
      message: 'Package saved successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({ message })
  }
}

const destroy = async (req, res) => {
  try {
    await Models.sequelize.transaction(async transaction => {
      const packageFound = await Models.Package.findByPk(req.params.id)
      if (!packageFound) {
        return res.status(200).json({
          message: 'Package not found'
        })
      }
      await packageFound.setItems([], {
        transaction
      })
      await packageFound.setHandlingUnits([], {
        transaction
      })
      await packageFound.destroy({ transaction })
    })
    res.status(200).send({
      message: 'package deleted succesfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}
const updateMultiple = async (req, res) => {
  try {
    await Models.Package.update(req.body.fields,
      {
        where: {
          id: req.body.packages
        }
      })
    res.status(200).send({
      message: 'packages updated succesfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}
const destroyMultiple = async (req, res) => {
  try {
    const packagesFound = await Models.Package.findAll({
      where: {
        id: req.body.packages
      }
    })
    if (!packagesFound) {
      return res.status(200).json({
        message: 'packages not found'
      })
    }
    await Models.PackageItems.destroy({
      where: {
        PackageId: req.body.packages
      }
    })
    await Models.HandlingUnitPackages.destroy({
      where: {
        PackageId: req.body.packages
      }
    })
    await Models.Package.destroy({
      where: {
        id: req.body.packages
      }
    })
    res.status(200).send({
      message: 'packages deleted succesfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}

module.exports = {
  updateMultiple,
  destroyMultiple,
  getAll,
  create,
  getById,
  update,
  destroy
}
