const Models = require('../models')

async function getAll (req, res) {
  try {
    const organizationTypes = await Models.OrganizationType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: organizationTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const organizationType = await Models.OrganizationType.build(req.body).save()
    res.status(200).send({
      data: organizationType,
      message: 'OrganizationType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
