/*
const Models = require('../models')
const { includeGet, includeUpdate } = require('./includes/rates')
const _ = require('lodash')

const getWithoutData = items => items.filter(itemObject =>
  _.isEmpty(itemObject) || _.isEmpty(itemObject, true))

const getItemsToBeLinked = items => items.filter(itemObject => itemObject.id)

const getItemsToBeCreated = items => items.filter(itemObject => !itemObject.id)

const getItemsWithInvalidData = items => (
  items.filter(itemObject => {
    const {
      id,
      quantity,
      ...rest
    } = itemObject
    return _.isEmpty(itemObject) ||
      (id && !quantity && !_.isEmpty(rest)) ||
      (!id && quantity && _.isEmpty(rest)) ||
      (id && quantity && !_.isEmpty(rest))
  })
)

async function getAll (req, res) {
  try {
    const findParams = {
      ...includeGet(Models),
      limit: _.get(req, 'query.limit', 25),
      offset: _.get(req, 'query.offset', 0)
    }
    const rates = await Models.Rate.findAll(findParams)
    res.status(200).send({ data: rates })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function getById (req, res) {
  try {
    const { id } = req.params
    const rate = await Models.Rate.findByPk(id, includeGet(Models))

    res.status(200).send({ data: rate })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const { header: {
      originCountry,
      destinationCountry
    }, data } = req.body

    const rateBody = {
      countryOfOrigin: originCountry,
      countryOfDestiny: destinationCountry,
      createdBy: req.user.id,
      updatedBy: req.user.id
    }

    let rateSaved

    await Models.sequelize.transaction(
      async transaction => {
        try {
          const rateSaved = await Models.Rate.create(rateBody, { transaction })

          const rateDetailsToCreate = data.map((itemObj, i) => ({
            RateId: rateSaved.id,
            validitySince: itemObj.validitySince,
            validityUntil: itemObj.validityUntil,
            originPort: itemObj.port,
            freeTime: itemObj.freeTime,
            isps: itemObj.isps,
            createdBy: req.user.id,
            updatedBy: req.user.id
          }))

          const rateDetailSaved = await Models.Package.bulkCreate(rateDetailsToCreate, { transaction, returning: true })

          let containerRatesToCreate

          for(let i = 0; i < rateDetailSaved.length;i++){

            let ratePortsToCreate = data[i].data.map((i,k)=>({
              destinationPort: i.origin,
              rateDetailsId: rateDetailSaved[i].id
            }))
          }

          rateDeailSaved.forEach(element => {

          });
        }
    res.status(200).send({
      data: contactSaved,
      message: 'Contact created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  getById,
  create,
}
*/
