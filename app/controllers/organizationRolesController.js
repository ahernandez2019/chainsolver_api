const Models = require('../models')

async function getAll (req, res) {
  try {
    const organizationRoles = await Models.OrganizationRole.findAll()
    res.status(200).send({ data: organizationRoles })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const organizationRole = await Models.OrganizationRole.build(req.body).save()
    res.status(200).send({
      data: organizationRole,
      message: 'OrganizationRole created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
