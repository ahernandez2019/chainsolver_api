const Models = require('../models')

async function getAll (req, res) {
  try {
    const WeightUnits = await Models.WeightUnit.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: WeightUnits })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const WeightUnit = await Models.WeightUnit.build(req.body).save()
    res.status(200).send({
      data: WeightUnit,
      message: 'WeightUnit created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
