const Models = require('../models')

async function getAll (req, res) {
  try {
    const gearTypes = await Models.GearType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: gearTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const gearType = await Models.GearType.build(req.body).save()
    res.status(200).send({
      data: gearType,
      message: 'GearType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
