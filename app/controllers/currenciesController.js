const Models = require('../models')
const _ = require('lodash')

async function getAll (req, res) {
  try {
    const limit = _.get(req, 'query.limit', 25)
    const offset = _.get(req, 'query.offset', 0)
    const currencies = await Models.Currency.findAll({ attributes: ['id', 'name'], limit, offset })
    res.status(200).send({ data: currencies })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const currency = await Models.Currency.build(req.body).save()
    res.status(200).send({
      data: currency,
      message: 'currency created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
