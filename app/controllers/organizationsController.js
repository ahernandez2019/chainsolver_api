/* eslint-disable camelcase */
const Models = require('../models')
const { includeGet, includeGetById, includeUpdate } = require('./includes/organizations')
const { setCity } = require('./utils/cities')
const _ = require('lodash')

const getAll = async (req, res) => {
  const limit = _.get(req, 'query.limit', 25)
  const offset = _.get(req, 'query.offset', 0)
  const findParams = {
    ...includeGet(Models, true),
    limit,
    offset
  }

  try {
    const organizations = await Models.Organization.findAll(findParams)
    res.status(200).send({ data: organizations })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const getById = async (req, res) => {
  try {
    const { id } = req.params
    const organization = await Models.Organization.findByPk(id, includeGetById(Models))

    res.status(200).send({ data: organization })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const reduceContacts = contacts => _.isArray(contacts) && !_.isEmpty(contacts) &&
  contacts.reduce((prevContacts, contact) => {
    return [
      ...prevContacts,
      {
        ...contact,
        Phones: [{ number: contact.phone }]
      }
    ]
  }, [])

const reduceLocations = (locations, hQLocation) => (_.isArray(locations) && !_.isEmpty(locations) &&
  locations.reduce((prevLocations, location) => ([
    ...prevLocations,
    {
      ...location,
      isHq: false,
      Address: {
        address: location.address,
        zipCode: location.hqZip,
        CityId: 1 // fix this. Still not come id from fe
      },
      Contacts: reduceContacts(location.contacts)
      // Phone: { // location does not have phone itself... yet
      //   number: location.phone
      // }
    }
  ]), hQLocation)) || hQLocation

const create = async (req, res) => {
  const { organization: {
    name,
    organizationType: OrganizationTypeId,
    phone: number,
    website,
    locations,
    bankAccName,
    bankAccNumber,
    bankName,
    bankAbn,
    bankSwift,
    bankAddress,
    bankCity,
    bankState,
    bankZip,
    bankCountry,
    billingAddress,
    billingCity,
    billingState,
    billingZip,
    billingCountry,
    billingPhone,
    billingFax,
    billingEmail,
    billingNotes,
    ...locationFields
  } } = req.body

  try {
    let organizationSaved
    await Models.sequelize.transaction(async transaction => {
      const organizationCity = await setCity(Models, locationFields.hqCity, locationFields.hqCountry, locationFields.hqState, transaction)

      const organizationBody = {
        name,
        OrganizationTypeId,
        website,
        bankAccName,
        bankAccNumber,
        bankName,
        bankAbn,
        bankSwift,
        bankAddress,
        bankCity,
        bankState,
        bankZip,
        bankCountry,
        billingAddress,
        billingCity,
        billingState,
        billingZip,
        billingCountry,
        billingPhone,
        billingFax,
        billingEmail,
        billingNotes,
        createdBy: 1, // user mock
        Locations: [
          {
            isHq: true,
            name: 'HQ',
            loadingDock: true,
            forklift: true,
            helpLoading: true,
            operationHours: '',
            needsAppointment: true,
            createdBy: 1, // user mock
            notes: locationFields.description,
            Address: {
              address: locationFields.address.address,
              zipCode: locationFields.hqZip,
              coordinates: locationFields.address && locationFields.address.lat && `${locationFields.address.lat}|${locationFields.address.lng}`,
              CityId: organizationCity
            },
            Phone: { number }
          }
        ]
      }

      organizationBody.Locations = reduceLocations(locations, organizationBody.Locations)

      organizationSaved = await Models.Organization.create(organizationBody, { ...includeUpdate(Models), transaction })
    })

    organizationSaved = await Models.Organization.findByPk(organizationSaved.id, { ...includeGetById(Models) })

    return res.status(200).send({
      data: organizationSaved,
      message: 'Organization created successfully'
    })
  } catch (error) {
    console.error('---error creating organization ---', error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const update = async (req, res) => {
  const { organization } = req.body
  const { id } = req.params

  let organizationSaved = await Models.Organization.findByPk(id, includeUpdate(Models))

  if (_.isEmpty(organizationSaved)) {
    return res.status(500).send('Error Updating Organization')
  }

  const {
    name,
    organizationType,
    phone,
    website,
    bankAccName,
    bankAccNumber,
    bankName,
    bankAbn,
    bankSwift,
    bankAddress,
    bankCity,
    bankState,
    bankZip,
    bankCountry,
    billingAddress,
    billingCity,
    billingState,
    billingZip,
    billingCountry,
    billingPhone,
    billingFax,
    billingEmail,
    billingNotes,
    ...locationFields
  } = organization

  try {
    await Models.sequelize.transaction(async transaction => {
      await organizationSaved.update({
        name,
        OrganizationTypeId: organizationType,
        website,
        bankAccName,
        bankAccNumber,
        bankName,
        bankAbn,
        bankSwift,
        bankAddress,
        bankCity,
        bankState,
        bankZip,
        bankCountry,
        billingAddress,
        billingCity,
        billingState,
        billingZip,
        billingCountry,
        billingPhone,
        billingFax,
        billingEmail,
        billingNotes
      }, { transaction })

      await organizationSaved.Locations[0].update({
        notes: locationFields.description
      }, { transaction })
      const organizationCity = await setCity(Models, locationFields.hqCity, locationFields.hqCountry, locationFields.hqState, transaction)

      if (locationFields.address && locationFields.address.lat) {
        await organizationSaved.Locations[0].Address.update({
          address: locationFields.address.address,
          zipCode: locationFields.hqZip,
          coordinates: `${locationFields.address.lat}|${locationFields.address.lng}`,
          CityId: organizationCity
        }, { transaction })
      }

      if (!organizationSaved.Locations[0].Phone) {
        await Models.Phone.create({
          LocationId: organizationSaved.Locations[0].id,
          number: phone,
          createdBy: 1, // use userId
          updatedBy: 1 // use userId
        }, { transaction })
      } else {
        await organizationSaved.Locations[0].Phone.update({
          number: phone
        }, { transaction })
      }
    })

    organizationSaved = await Models.Organization.findByPk(organizationSaved.id, includeGetById(Models))

    res.status(200).send({
      data: organizationSaved,
      message: 'Organization updated successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const destroy = async (req, res) => {
  const id = _.parseInt(req.params.id) || 0
  const reassign = _.parseInt(req.query.reassign)

  const organizationSaved = await Models.Organization.findByPk(id)

  if (_.isEmpty(organizationSaved)) {
    return res.status(500).send({
      message: 'Organization not exists'
    })
  }

  if (!isNaN(reassign)) {
    if (reassign === _.parseInt(id) || reassign === 0) {
      return res.status(500).send({
        message: 'ID conflicts'
      })
    }

    try {
      await Models.sequelize.query('UPDATE "Locations" set "OrganizationId"=:organizationId where "OrganizationId"=:id', {
        replacements: {
          id,
          organizationId: reassign
        },
        type: Models.sequelize.QueryTypes.UPDATE
      })
    } catch (error) {
      console.log('error', error)
      return res.status(500).send({
        message: 'Locations cant be updated'
      })
    }
  }

  try {
    await Models.Item.update({
      supplier: null
    },
    {
      where: {
        supplier: id
      }
    })
    await Models.Location.update({
      OrganizationId: null
    },
    {
      where: {
        OrganizationId: id
      }
    })
    await Models.OrganizationMember.update({
      OrganizationId: null
    },
    {
      where: {
        OrganizationId: id
      }
    })
    await Models.Item.update({
      manufacturer: null
    },
    {
      where: {
        manufacturer: id
      }
    })
    await organizationSaved.destroy()
    res.status(200).send({
      message: 'Organization deleted successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy
}
