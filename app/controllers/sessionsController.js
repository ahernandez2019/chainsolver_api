const Models = require('../models')

async function getAll (req, res) {
  try {
    const sessions = await Models.Session.findAll()
    res.status(200).send({ data: sessions })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll
}
