const Models = require('../models')

async function getAll (req, res) {
  try {
    const itemTypes = await Models.ItemType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: itemTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const itemType = await Models.ItemType.build(req.body).save()
    res.status(200).send({
      data: itemType,
      message: 'itemType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
