
const _ = require('lodash')

// models
const Models = require('../models')

// includes
const {
  includeGet
} = require('./includes/items')

const getAll = async (req, res) => {
  const where = {}
  if (req.query.OrganizationId) {
    where.OrganizationId = req.query.OrganizationId
  }
  if (req.query.LocationId) {
    where.manufacturerLocationId = req.query.LocationId
  }
  const limit = _.get(req, 'query.limit', 25)
  const offset = _.get(req, 'query.offset', 0)
  const findParams = {
    ...includeGet(Models),
    limit,
    offset,
    where
  }
  try {
    const items = await Models.Item.findAndCountAll(findParams)
    const { count, rows } = items
    res.status(200).send({
      count,
      rows
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}
const updateMultiple = async (req, res) => {
  try {
    await Models.Item.update(req.body.fields,
      {
        where: {
          id: req.body.items
        }
      })
    res.status(200).send({
      message: 'items updated succesfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}
const destroyMultiple = async (req, res) => {
  try {
    const itemsFound = await Models.Item.findAll({
      where: {
        id: req.body.items
      }
    })
    if (!itemsFound) {
      return res.status(200).json({
        message: 'items not found'
      })
    }
    await Models.HazmatItems.destroy({
      where: {
        ItemId: req.body.items
      }
    })
    await Models.CustomFieldItems.destroy({
      where: {
        ItemId: req.body.items
      }
    })
    await Models.PackageItems.destroy({
      where: {
        ItemId: req.body.items
      }
    })
    await Models.Item.destroy({
      where: {
        id: req.body.items
      }
    })
    res.status(200).send({
      message: 'items successfully deleted'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}
/**
 * TODO: fix barCode and pictureOfProduct  when uploadFiles is fixed
*/
const getCustomFieldsWithoutData = customFields => customFields.filter(customFieldObject =>
  _.isEmpty(customFieldObject))

const getCustomFieldsToBeLinked = customFields => customFields.filter(customFieldObject => customFieldObject.id)

const getCustomFieldsToBeCreated = customFields => customFields.filter(customFieldObject => !customFieldObject.id)
const getCustomFieldsWithInvalidData = customFields => (
  customFields.filter(customFieldObject => {
    const { id, value, ...rest } = customFieldObject
    return _.isEmpty(customFieldObject) ||
      (id && !value && !_.isEmpty(rest)) ||
      (!id && value && _.isEmpty(rest)) ||
      (id && value && !_.isEmpty(rest))
  })
)
const getHazmatsWithInvalidData = hazmats => (
  hazmats.filter(hazmatObject => {
    const { id, enable, ...rest } = hazmatObject
    return _.isEmpty(hazmatObject) ||
      (id && !enable && !_.isEmpty(rest)) ||
      (!id && enable && _.isEmpty(rest)) ||
      (id && enable && !_.isEmpty(rest))
  })
)

const getHazmatsWithoutData = hazmats => hazmats.filter(hazmatObject =>
  _.isEmpty(hazmatObject))

const getHazmatsToBeLinked = hazmats => hazmats.filter(hazmatObject => hazmatObject.id)

const getHazmatsToBeCreated = hazmats => hazmats.filter(hazmatObject => !hazmatObject.id)

const create = async (req, res) => {
  try {
    if (!req.body.item.OrganizationId) {
      req.body.item.OrganizationId = req.body.item.manufacturer
    }
    if (!req.body.item.supplier && req.body.item.OrganizationId) {
      req.body.item.supplier = req.body.item.OrganizationId
    }
    const { item: { hazmats, customFields, ...itemData } } = req.body

    if (hazmats) {
      const hazmatsWithInvalidData = getHazmatsWithInvalidData(hazmats)
      const hazmatsWithoutData = getHazmatsWithoutData(hazmats)
      let message
      if (hazmatsWithoutData.length) {
        message = `you can't create a hazmat with empty data`
      } else if (hazmatsWithInvalidData.length) {
        message = `Invalid hazmat data sent. Not allowed to send (id + quantity + pacakge's data)
          or (id + hazmat's data) `
      }
      if (message) {
        return res.status(400).json({
          data: {},
          message: message
        })
      }
    }
    if (customFields) {
      const customFieldsWithInvalidData = getCustomFieldsWithInvalidData(customFields)
      const customFieldsWithoutData = getCustomFieldsWithoutData(customFields)
      let message
      if (customFieldsWithoutData.length) {
        message = `you can't create a customFields with empty data`
      } else if (customFieldsWithInvalidData.length) {
        message = `Invalid customFields data sent. Not allowed to send (id + value + customfield's data)
          or (id + customFields's data) `
      }
      if (message) {
        return res.status(400).json({
          data: {},
          message: message
        })
      }
    }

    let itemSaved
    await Models.sequelize.transaction(async transaction => {
      itemSaved = await Models.Item.create(itemData,
        {
          ...includeGet(Models),
          transaction
        })

      if (hazmats) {
        const hazmatsToCreate = getHazmatsToBeCreated(hazmats)
        const hazmatsToLink = getHazmatsToBeLinked(hazmats).map(hazmatObject => ({
          id: hazmatObject.id,
          enable: (hazmatObject.enable || true)
        }))
        const hazmatsResult = await Models.HazmatClass.bulkCreate(hazmatsToCreate, { transaction, returning: true })

        let hazmatsSavedWithEnable = hazmatsResult.map((hazmatObj, i) => ({
          id: hazmatObj.id,
          enable: (hazmatsToCreate[i].enable || true)
        }))

        hazmatsSavedWithEnable = [...hazmatsSavedWithEnable, ...hazmatsToLink]

        const itemHazmatsRecords = hazmatsSavedWithEnable.map((hazmatObject, i) => (
          { HazmatClassId: hazmatObject.id, ItemId: itemSaved.id, enable: hazmatObject.enable }
        ))
        await Models.HazmatItems.bulkCreate(itemHazmatsRecords, { transaction })
      }
      if (customFields) {
        const customFieldsToCreate = getCustomFieldsToBeCreated(customFields)
        const customFieldsToLink = getCustomFieldsToBeLinked(customFields).map(customFieldsObject => ({
          id: customFieldsObject.id,
          value: (customFieldsObject.value || 'null')
        }))
        const customFieldsObjectResult = await Models.CustomField.bulkCreate(customFieldsToCreate, { transaction, returning: true })

        let customFieldsSavedWithValue = customFieldsObjectResult.map((customFieldsObj, i) => ({
          id: customFieldsObj.id,
          value: (customFieldsToCreate[i].value || 'null')
        }))

        customFieldsSavedWithValue = [...customFieldsSavedWithValue, ...customFieldsToLink]

        const itemCustomFieldsRecords = customFieldsSavedWithValue.map((customFieldObject, i) => (
          { CustomFieldId: customFieldObject.id, ItemId: itemSaved.id, value: customFieldObject.value }
        ))
        await Models.CustomFieldItems.bulkCreate(itemCustomFieldsRecords, { transaction })
      }
    })

    const item = await Models.Item.findByPk(itemSaved.id, includeGet(Models))
    res.status(200).json({
      data: item,
      message: 'Item created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const getById = async (req, res) => {
  const id = _.parseInt(req.params.id)
  const item = await Models.Item.findByPk(id, includeGet(Models))
  if (_.isEmpty(item)) {
    return res.status(500).send({
      message: 'Item not exists'
    })
  }
  try {
    res.status(200).send({
      data: item,
      message: 'item found'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

const update = async (req, res) => {
  try {
    const { item: { hazmats, customFields, ...itemData } } = req.body
    let id = _.parseInt(req.params.id)
    if (hazmats) {
      const hazmatsWithoutData = getHazmatsWithoutData(hazmats)
      if (hazmatsWithoutData.length) {
        throw new Error(`you can't create a hazmat with empty data`)
      }
    }
    if (customFields) {
      const customFieldsWithoutData = getCustomFieldsWithoutData(hazmats)
      if (customFieldsWithoutData.length) {
        throw new Error(`you can't create a customField with empty data`)
      }
    }
    let itemFound
    await Models.sequelize.transaction(async transaction => {
      const result = await Models.Item.update(itemData, { transaction, returning: true, where: { id } })
      const [rowsAffected, itemResult] = result
      const itemSaved = itemResult[0]
      if (!rowsAffected) {
        return res.status(200).json({ status: 200, data: itemResult })
      }
      if (hazmats) {
        const hazmatsToCreate = getHazmatsToBeCreated(hazmats)

        const hazmatsToLink = getHazmatsToBeLinked(hazmats).map(hazmatObject => ({
          id: hazmatObject.id,
          enable: (hazmatObject.enable || true)
        }))
        await itemSaved.setHazmatClasses([], { transaction })

        const hazmatsResult = await Models.HazmatClass.bulkCreate(hazmatsToCreate, { transaction, returning: true })

        let hazmatsSavedWithEnable = hazmatsResult.map((hazmatObj, i) => ({
          id: hazmatObj.id,
          enable: (hazmatsToCreate[i].enable || 1)
        }))

        hazmatsSavedWithEnable = [...hazmatsSavedWithEnable, ...hazmatsToLink]

        const itemHazmatsRecords = hazmatsSavedWithEnable.map((hazmatObject, i) => (
          { HazmatClassId: hazmatObject.id, ItemId: itemSaved.id, enable: hazmatObject.enable }
        ))
        await Models.HazmatItems.bulkCreate(itemHazmatsRecords, { transaction })
      }
      if (customFields) {
        const customFieldsToCreate = getCustomFieldsToBeCreated(customFields)

        const customFieldsToLink = getCustomFieldsToBeLinked(customFields).map(customFieldObject => ({
          id: customFieldObject.id,
          value: (customFieldObject.value || 'null')
        }))
        await itemSaved.setCustomFields([], { transaction })

        const customFieldsResult = await Models.CustomField.bulkCreate(customFieldsToCreate, { transaction, returning: true })

        let customFieldsSavedWithValue = customFieldsResult.map((customFieldObj, i) => ({
          id: customFieldObj.id,
          value: (customFieldsToCreate[i].value || 'null')
        }))

        customFieldsSavedWithValue = [...customFieldsSavedWithValue, ...customFieldsToLink]

        const itemCustomFieldsRecords = customFieldsSavedWithValue.map((customFieldObject, i) => (
          { CustomFieldId: customFieldObject.id, ItemId: itemSaved.id, value: customFieldObject.value }
        ))
        await Models.CustomFieldItems.bulkCreate(itemCustomFieldsRecords, { transaction })
      }
    })
    itemFound = await Models.Item.findByPk(id, includeGet(Models))
    res.status(200).json({
      data: itemFound,
      message: 'item saved successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}

const destroy = async (req, res) => {
  const id = _.parseInt(req.params.id)
  const itemSaved = await Models.Item.findByPk(id)
  if (_.isEmpty(itemSaved)) {
    return res.status().send({
      message: 'Item not exists'
    })
  }
  try {
    await Models.PackageItems.destroy({
      where: {
        ItemId: id
      }
    })
    await Models.CustomFieldItems.destroy({
      where: {
        ItemId: id
      }
    })
    await Models.HazmatItems.destroy({
      where: {
        ItemId: id
      }
    })
    await itemSaved.destroy()
    res.status(200).send({
      message: 'Item deleted successfully'
    })
  } catch (error) {
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create,
  getById,
  update,
  destroy,
  updateMultiple,
  destroyMultiple
}
