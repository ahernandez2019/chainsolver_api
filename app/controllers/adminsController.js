const Models = require('../models')

async function getAll (req, res) {
  try {
    const admins = await Models.Admin.findAll()
    res.status(200).send({ data: admins })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll
}
