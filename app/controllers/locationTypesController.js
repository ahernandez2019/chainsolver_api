const Models = require('../models')

async function getAll (req, res) {
  try {
    const locationTypes = await Models.LocationType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: locationTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const locationType = await Models.LocationType.build(req.body).save()
    res.status(200).send({
      data: locationType,
      message: 'LocationType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
