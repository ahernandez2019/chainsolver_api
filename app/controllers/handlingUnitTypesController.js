const Models = require('../models')

async function getAll (req, res) {
  try {
    const handlingUnitTypes = await Models.UnitType.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: handlingUnitTypes })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const handlingUnitType = await Models.UnitType.build(req.body).save()
    res.status(200).send({
      data: handlingUnitType,
      message: 'handlingUnitType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
