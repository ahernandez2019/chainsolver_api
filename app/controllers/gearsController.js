const Models = require('../models')
const _ = require('lodash')
const {
  includeGet
} = require('./includes/gears')

const getAll = async (req, res) => {
  const limit = _.get(req, 'query.limit', 25)
  const offset = _.get(req, 'query.offset', 0)

  let findParams = {
    ...includeGet(Models),
    limit,
    offset
  }
  try {
    if (req.query.GearTypeId !== undefined) {
      findParams = { ...findParams, where: { 'GearTypeId': req.query.GearTypeId } }
    }
    const gears = await Models.Gear.findAndCountAll(findParams)
    const { count, rows } = gears
    res.status(200).send({
      count,
      rows
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

const create = async (req, res) => {
  try {
    const gearSaved = await Models.Gear.build(req.body).save()
    const gear = await Models.Gear.findByPk(gearSaved.id, includeGet(Models))
    res.status(200).send({
      data: gear,
      message: 'Equipment created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

const getById = async (req, res) => {
  try {
    const id = _.parseInt(req.params.id)
    const gear = await Models.Gear.findByPk(id, includeGet(Models))
    if (!gear) {
      return res.status(404).json({ message: 'Equipment not found' })
    }
    res.status(200).send({
      data: gear,
      message: 'Equipment found successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

const update = async (req, res) => {
  try {
    const id = _.parseInt(req.params.id)
    const gearSaved = await Models.Gear.findByPk(id)
    await gearSaved.update(req.body)
    res.status(200).send({
      data: gearSaved,
      message: 'Equipment updated successfully'
    })
  } catch (error) {
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

const destroy = async (req, res) => {
  try {
    const id = _.parseInt(req.params.id)
    const gearSaved = await Models.Gear.findByPk(id)
    await gearSaved.destroy()
    res.status(200).send({
      message: 'Equipment deleted successfully'
    })
  } catch (error) {
    const message = error.toString()
    res.status(500).send({
      message
    })
  }
}

module.exports = {
  getAll,
  create,
  getById,
  update,
  destroy
}
