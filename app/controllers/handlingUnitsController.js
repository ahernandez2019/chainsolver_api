const Models = require('../models')
const { includeGet } = require('./includes/handlingUnits')
const _ = require('lodash')

const getPackagesWithInvalidData = packages => (
  packages.filter(packageObject => {
    const { id, quantity, ...rest } = packageObject
    return _.isEmpty(packageObject) ||
      (id && !quantity && !_.isEmpty(rest)) ||
      (!id && quantity && _.isEmpty(rest)) ||
      (id && quantity && !_.isEmpty(rest))
  })
)

const getPackagesWithoutData = packages => packages.filter(packageObject =>
  _.isEmpty(packageObject))

const getPackagesToBeLinked = packages => packages.filter(packageObject => packageObject.id)

const getPackagesToBeCreated = packages => packages.filter(packageObject => !packageObject.id)

const getAll = async (req, res) => {
  try {
    const where = {}
    if (req.query.OrganizationId) {
      where.OrganizationId = req.query.OrganizationId
    }
    if (req.query.LocationId) {
      where.manufacturerLocationId = req.query.LocationId
    }
    const itemsIds = await Models.Item.findAll({
      where
    }).map(i => i.id)
    const packages = await Models.PackageItems.findAll({
      where: {
        ItemId: {
          in: itemsIds
        }
      }
    })
    const packagesIds = _.uniqBy(packages, 'PackageId').map(p => p.PackageId)

    const hUP = await Models.HandlingUnitPackages.findAll({
      where: {
        PackageId: {
          in: packagesIds
        }
      }
    }).map(h => h.HandlingUnitId)
    const handlingUnitsIds = _.uniqBy(hUP, 'id')
    const findParams = {
      ...includeGet(Models),
      limit: _.get(req, 'query.limit', 25),
      offset: _.get(req, 'query.offset', 0),
      where: {
        id: {
          in: handlingUnitsIds
        }
      }
    }
    const handlingUnits = await Models.HandlingUnit.findAndCountAll(findParams)
    const { count, rows } = handlingUnits
    res.setHeader('X-Total-Count', count)
    res.status(200).json({
      count,
      rows
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const getById = async (req, res) => {
  try {
    const { id } = req.params
    const handlingUnit = await Models.HandlingUnit.findByPk(id, includeGet(Models))
    if (!handlingUnit) {
      return res.status(404).json({ message: 'HandlingUnit not found' })
    }

    res.status(200).json({ data: handlingUnit, message: 'Handling unit found' })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const create = async (req, res) => {
  try {
    const { handlingUnit: { packages, ...handlingUnitData } } = req.body

    if (packages) {
      const packagesWithInvalidData = getPackagesWithInvalidData(packages)
      const packagesWithoutData = getPackagesWithoutData(packages)
      let message
      if (packagesWithoutData.length) {
        message = `you can't create a package with empty data`
      } else if (packagesWithInvalidData.length) {
        message = `Invalid package data sent. Not allowed to send (id + quantity + pacakge's data)
          or (id + package's data) `
      }
      if (message) {
        return res.status(400).json({
          data: {},
          message: message
        })
      }
    } else if (!packages || !packages[0]) {
      return res.status(500).json({
        message: 'cannot create handling units without packages'
      })
    }
    let handlingUnitSaved
    await Models.sequelize.transaction(async transaction => {
      handlingUnitSaved = await Models.HandlingUnit.create(handlingUnitData,
        {
          ...includeGet(Models),
          transaction
        })

      if (packages) {
        const packagesToCreate = getPackagesToBeCreated(packages)
        const packagesToLink = getPackagesToBeLinked(packages).map(packageObject => ({
          id: packageObject.id,
          quantity: (packageObject.quantity || 1)
        }))
        const packagesResult = await Models.Package.bulkCreate(packagesToCreate, { transaction, returning: true })

        let packagesSavedWithQuantity = packagesResult.map((packageObj, i) => ({
          id: packageObj.id,
          quantity: (packagesToCreate[i].quantity || 1)
        }))

        packagesSavedWithQuantity = [...packagesSavedWithQuantity, ...packagesToLink]

        const handlingUnitPackagesRecords = packagesSavedWithQuantity.map((packageObject, i) => (
          { HandlingUnitId: handlingUnitSaved.id, PackageId: packageObject.id, quantity: packageObject.quantity }
        ))
        await Models.HandlingUnitPackages.bulkCreate(handlingUnitPackagesRecords, { transaction })
      }
    })

    const handlingUnit = await Models.HandlingUnit.findByPk(handlingUnitSaved.id, includeGet(Models))
    res.status(200).json({
      data: handlingUnit,
      message: 'HandlingUnit created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const update = async (req, res) => {
  try {
    const { handlingUnit: { packages, ...handlingUnitData } } = req.body

    if (packages) {
      const packagesWithoutData = getPackagesWithoutData(packages)
      if (packagesWithoutData.length) {
        return res.status(400).json({
          data: {},
          message: `you can't create a package with empty data`
        })
      }
    }

    let handlingUnitFound
    await Models.sequelize.transaction(async transaction => {
      const result = await Models.HandlingUnit.update(handlingUnitData, { transaction, returning: true, where: { id: req.params.id } })
      const [rowsAffected, handlingUnitResult] = result
      const handlingUnitSaved = handlingUnitResult[0]
      if (!rowsAffected) {
        return res.status(200).json({ data: handlingUnitResult })
      }

      if (packages) {
        const packagesToCreate = getPackagesToBeCreated(packages)
        const packagesToLink = getPackagesToBeLinked(packages)
        await handlingUnitSaved.setPackages([], { transaction })

        const packagesResult = await Models.Package.bulkCreate(packagesToCreate, { transaction, returning: true })

        const handlingUnitPackagesRecords = packagesResult.map((packageObject, i) => (
          { 'HandlingUnitId': handlingUnitSaved.id, 'PackageId': packageObject.id, 'quantity': packagesToCreate[i].quantity || 1 }
        ))

        const handlingUnitPackagesToLinkRecords = packagesToLink.map((packageObject) => (
          { 'HandlingUnitId': handlingUnitSaved.id, 'PackageId': packageObject.id, 'quantity': packageObject.quantity || 1 }
        ))
        await Models.HandlingUnitPackages.bulkCreate([...handlingUnitPackagesRecords, ...handlingUnitPackagesToLinkRecords], { transaction })
      }
    })

    handlingUnitFound = await Models.HandlingUnit.findByPk(req.params.id, includeGet(Models))

    res.status(200).json({
      data: handlingUnitFound,
      message: 'HandlingUnit saved successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

const destroy = async (req, res) => {
  try {
    await Models.sequelize.transaction(async transaction => {
      const handlingUnit = await Models.HandlingUnit.findByPk(req.params.id)
      if (!handlingUnit) {
        return res.status(404).json({ message: 'HandlingUnit not found' })
      }
      await handlingUnit.setPackages([], { transaction })
      await handlingUnit.destroy({ transaction })
    })
    res.status(200).send({
      message: 'handling unit successfuly deleted'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}
const updateMultiple = async (req, res) => {
  try {
    await Models.HandlingUnit.update(req.body.fields,
      {
        where: {
          id: req.body.handlingUnits
        }
      })
    res.status(200).send({
      message: 'handling units updated succesfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}
const destroyMultiple = async (req, res) => {
  try {
    const handlingUnitsFound = await Models.HandlingUnit.findAll({
      where: {
        id: req.body.handlingUnits
      }
    })
    if (!handlingUnitsFound) {
      return res.status(200).json({
        message: 'handling units not found'
      })
    }
    await Models.HandlingUnitPackages.destroy({
      where: {
        HandlingUnitId: req.body.handlingUnits
      }
    })
    await Models.HandlingUnit.destroy({
      where: {
        id: req.body.handlingUnits
      }
    })
    res.status(200).send({
      message: 'handling units successfuly deleted'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(400).send({
      message
    })
  }
}
module.exports = {
  destroyMultiple,
  updateMultiple,
  getAll,
  getById,
  create,
  update,
  destroy
}
