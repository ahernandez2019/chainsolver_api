const includeGet = Models => ({
  order: ['id', 'DESC'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.Country,
      as: 'CountryOfOrigin',
      attributes: ['id', 'name']
    },
    {
      model: Models.Country,
      as: 'CountryOfDestiny',
      attributes: ['id', 'name']
    },
    {
      model: Models.RateDetails,
      attributes: ['validitySince', 'validityUntil'],
      include: [
        {
          model: Models.Port,
          as: 'OriginPort',
          attributes: ['name']
        },
        {
          model: Models.RatePorts,
          include: [
            {
              model: Models.Port,
              as: 'DestinationPort',
              attributes: ['name']
            },
            {
              model: Models.ContainerRates,
              attributes: ['value'],
              include: [
                {
                  model: Models.ContainerType,
                  as: 'ContainerType',
                  attributes: ['size', 'type']
                },
                {
                  model: Models.Currency,
                  as: 'Currency',
                  attributes: ['name', 'label', 'symbol']
                }
              ]
            }
          ]
        }
      ]
    }
  ]
})

const includeUpdate = Models => ({
  include: [
    Models.Phone
  ]
})

module.exports = {
  includeGet,
  includeUpdate
}
