const includeGet = Models => ({
  order: ['id'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.GearType,
      attributes: ['id', 'name']
    }
  ]
})

module.exports = {
  includeGet
}
