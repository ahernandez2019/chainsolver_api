const includeGet = Models => ({
  order: ['id'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.Location,
      include: [
        Models.Organization
      ]
    },
    {
      model: Models.Phone,
      attributes: ['number']
    },
    {
      model: Models.Address,
      attributes: ['address', 'zipCode'],
      include: [
        {
          model: Models.City,
          attributes: ['name'],
          include: [
            {
              model: Models.State,
              attributes: ['name'],
              include: [
                {
                  model: Models.Country,
                  attributes: ['name']
                }
              ]
            }
          ]
        }
      ]
    }
  ]
})

const includeUpdate = Models => ({
  include: [
    Models.Phone
  ]
})

module.exports = {
  includeGet,
  includeUpdate
}
