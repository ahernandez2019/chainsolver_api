const includeGet = Models => ({
  order: ['id'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.Country,
      as: 'Country',
      attributes: ['name']
    },
    {
      model: Models.UnitType,
      attributes: ['name']
    },
    {
      model: Models.LengthUnit,
      attributes: ['id', 'name'],
      as: 'LengthUnit'
    },
    {
      model: Models.WeightUnit,
      attributes: ['id', 'name'],
      as: 'WeightUnit'
    },
    {
      model: Models.Package,
      as: 'Packages',
      through: 'HandlingUnitPackages',
      include: [{
        model: Models.LengthUnit,
        attributes: ['id', 'name'],
        as: 'LengthUnit'
      },
      {
        model: Models.WeightUnit,
        attributes: ['id', 'name'],
        as: 'WeightUnit'
      },
      {
        model: Models.Item,
        as: 'Items',
        through: 'PackageItems',
        include: [
          {
            model: Models.LengthUnit,
            attributes: ['id', 'name'],
            as: 'LengthUnit'
          },
          {
            model: Models.WeightUnit,
            attributes: ['id', 'name'],
            as: 'WeightUnit'
          },
          {
            model: Models.Country,
            attributes: ['id', 'name']
          },
          {
            model: Models.HazmatClass,
            as: 'HazmatClasses',
            attributes: ['id', 'name'],
            through: 'HazmatItems'
          }
        ]
      }
      ]
    }
  ]
})

module.exports = {
  includeGet
}
