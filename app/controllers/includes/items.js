const includeGet = Models => ({
  order: [
    ['id', 'DESC']
  ],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.LengthUnit,
      attributes: ['id', 'name'],
      as: 'LengthUnit'
    },
    {
      model: Models.WeightUnit,
      attributes: ['id', 'name'],
      as: 'WeightUnit'
    },
    {
      model: Models.Country,
      attributes: ['name']
    },
    {
      model: Models.ItemCategory,
      attributes: ['name']
    },
    {
      model: Models.CustomField,
      as: 'CustomFields',
      through: 'CustomFieldItems'
    },
    {
      model: Models.HazmatClass,
      as: 'HazmatClasses',
      through: 'HazmatItems'
    },
    {
      model: Models.PackageType,
      attributes: ['name']
    },
    {
      model: Models.Organization,
      as: 'Supplier',
      attributes: ['name', 'id']
    },
    {
      model: Models.Organization,
      as: 'Manufacturer',
      attributes: ['name', 'id']
    },
    {
      model: Models.PackingType,
      attributes: ['name']
    }
  ]
})

module.exports = {
  includeGet
}
