const includeGet = Models => ({
  order: ['id'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    Models.Contact,
    {
      model: Models.Phone,
      attributes: ['id', 'number']
    },
    {
      model: Models.Address,
      attributes: ['id', 'address', 'zipCode', 'coordinates'],
      include: [
        {
          model: Models.City,
          attributes: ['id', 'name'],
          include: [
            {
              model: Models.State,
              attributes: ['name'],
              include: [
                {
                  model: Models.Country,
                  attributes: ['name']
                }
              ]
            }
          ]
        }
      ]
    },
    {
      model: Models.LocationType,
      attributes: ['id', 'name']
    }
  ]
})

const includeUpdate = Models => ({
  include: [
    Models.LocationType,
    Models.Organization,
    Models.Phone,
    {
      model: Models.Address,
      include: [
        {
          model: Models.City,
          include: [
            {
              model: Models.State,
              include: [ Models.Country ]
            }
          ]
        }
      ]
    }
  ]
})

module.exports = {
  includeGet,
  includeUpdate
}
