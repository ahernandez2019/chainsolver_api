const includeGet = (Models, isHq) => ({
  order: ['id'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.Location,
      attributes: ['id', 'name', 'notes', 'isHq'],
      where: {
        isHq: true
      },
      include: locationProperties(Models)
    },
    {
      model: Models.OrganizationType,
      attributes: ['id', 'name']
    }
  ]
})

const includeGetById = (Models) => ({
  order: ['id'],
  include: [
    {
      model: Models.User,
      as: 'CreatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.User,
      as: 'UpdatedBy',
      attributes: ['id', 'name']
    },
    {
      model: Models.Location,
      attributes: ['id', 'name', 'notes'],
      where: {
        isHq: true
      },
      include: locationProperties(Models)
    },
    {
      model: Models.OrganizationType,
      attributes: ['id', 'name']
    }
  ]
})

const includeUpdate = Models => ({
  include: [
    Models.OrganizationType,
    {
      model: Models.Location,
      where: {
        isHq: true
      },
      include: [
        {
          model: Models.Contact,
          include: [
            Models.Phone,
            Models.Address
          ]
        },
        Models.Phone,
        Models.Address
      ]
    }
  ]
})

const locationProperties = (Models) => ([
  {
    model: Models.Phone,
    attributes: ['id', 'number']
  },
  {
    model: Models.Address,
    attributes: ['id', 'address', 'zipCode', 'coordinates'],
    include: [
      {
        model: Models.City,
        attributes: ['id', 'name'],
        include: [
          {
            model: Models.State,
            attributes: ['name'],
            include: [
              {
                model: Models.Country,
                attributes: ['name']
              }
            ]
          }
        ]
      }
    ]
  }
])

module.exports = {
  includeGet,
  includeGetById,
  includeUpdate
}
