const Models = require('../models')
const { includeGet, includeUpdate } = require('./includes/contacts')
const _ = require('lodash')

async function getAll (req, res) {
  try {
    const findParams = {
      ...includeGet(Models),
      limit: _.get(req, 'query.limit', 25),
      offset: _.get(req, 'query.offset', 0)
    }

    if (req.query.locationId !== undefined) {
      _.set(findParams, 'where.LocationId', req.query.locationId)
    }

    const contacts = await Models.Contact.findAll(findParams)
    res.status(200).send({ data: contacts })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function getById (req, res) {
  try {
    const { id } = req.params
    const contact = await Models.Contact.findByPk(id, includeGet(Models))

    res.status(200).send({ data: contact })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const { contact: {
      name,
      title,
      organizationName: OrganizationId,
      locationName: LocationId,
      phone,
      email
    } } = req.body

    const contactBody = {
      name,
      title,
      email,
      OrganizationId,
      LocationId
    }

    let contactSaved
    await Models.sequelize.transaction(async transaction => {
      contactSaved = await Models.Contact.create(contactBody, { transaction })

      const phoneBody = {
        number: phone,
        ContactId: contactSaved.id
      }

      await Models.Phone.create(phoneBody, { transaction })
    })

    contactSaved = await Models.Contact.findByPk(contactSaved.id, includeGet(Models))

    res.status(200).send({
      data: contactSaved,
      message: 'Contact created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function update (req, res) {
  try {
    const { id } = req.params
    const { contact: {
      name,
      title,
      organizationName: OrganizationId,
      locationName: LocationId,
      phone,
      email
    } } = req.body

    const contactBody = {
      name,
      title,
      email,
      OrganizationId,
      LocationId
    }

    let contactSaved

    contactSaved = await Models.Contact.findByPk(id, includeUpdate(Models))
    await Models.sequelize.transaction(async transaction => {
      await contactSaved.update(contactBody, { transaction })
      await contactSaved.Phones[0].update({ number: phone }, { transaction })
    })

    contactSaved = await Models.Contact.findByPk(id, includeGet(Models))

    res.status(200).send({
      data: contactSaved,
      message: 'Contact updated successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function destroy (req, res) {
  try {
    const { id } = req.params
    const contactSaved = await Models.Contact.findByPk(id)
    await Models.sequelize.transaction(async transaction => {
      await contactSaved.setPhones()
      await contactSaved.setAddresses()
      await contactSaved.destroy()
    })
    res.status(200).send({
      data: contactSaved,
      message: 'Contact deleted successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy
}
