const Models = require('../models')

async function getAll (req, res) {
  try {
    const phones = await Models.Phone.findAll()
    res.status(200).send({ data: phones })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const phone = await Models.Phone.build(req.body).save()

    res.status(200).send({
      data: phone,
      message: 'Phone created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
