const Models = require('../models')

async function getAll (req, res) {
  try {
    const customFields = await Models.CustomField.findAll({
      attributes: ['id', 'name']
    })
    res.status(200).send({ data: customFields })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

async function create (req, res) {
  try {
    const customField = await Models.CustomField.build(req.body).save()
    res.status(200).send({
      data: customField,
      message: 'GearType created successfully'
    })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll,
  create
}
