// const Op = require('sequelize').Op

const setCountry = async (models, country, transaction) => {
  let Country = await models.Country.findOne({
    where: {
      name: country
    },
    raw: true
  })

  if (!Country) {
    try {
      Country = await models.Country.create({
        name: country,
        createdBy: 1,
        updatedBy: 1
      }, { transaction, raw: true })
    } catch (error) {
      console.log('----error creating country----', error)
    }
  }
  return Country.id
}

const setState = async (models, state, CountryId, transaction) => {
  let State = await models.State.findOne({
    where: {
      name: state
    }
  }, { raw: true })

  if (!State) {
    try {
      State = await models.State.create({
        name: state,
        CountryId,
        createdBy: 1,
        updatedBy: 1
      }, { transaction, raw: true })
    } catch (error) {
      console.log('---error creating state---', error)
    }
  }
  return State.id
}

const setCity = async (models, city, country, state, transaction) => {
  let City = await models.City.findOne({
    where: {
      name: city
    }
  }, { raw: true })

  if (!City) {
    const CountryId = await setCountry(models, country, transaction)
    const StateId = await setState(models, state, CountryId, transaction)

    City = await models.City.create({
      name: city,
      StateId,
      CountryId,
      createdBy: 1,
      updatedBy: 1
    }, { transaction })
  }

  return City.id
}

module.exports = {
  setCity
}
