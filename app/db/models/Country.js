const country = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    iso3: {
      type: DataTypes.STRING
    },
    iso2: {
      type: DataTypes.STRING
    },
    countrycode: {
      type: DataTypes.INTEGER
    },
    phonecode: {
      type: DataTypes.STRING
    },
    capital: {
      type: DataTypes.STRING
    },
    currency: {
      type: DataTypes.STRING
    },
    flag: {
      type: DataTypes.BOOLEAN
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Country.associate = models => {
    Country.hasMany(models.State)
    Country.hasMany(models.City)
    Country.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Country.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Country
}

module.exports = country
