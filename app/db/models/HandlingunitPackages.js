'use strict'
module.exports = (sequelize, DataTypes) => {
  const HandlingUnitPackages = sequelize.define('HandlingUnitPackages', {
    quantity: {
      type: DataTypes.DECIMAL,
      required: true
    }
  }, { timestamps: true })
  HandlingUnitPackages.associate = models => {
    HandlingUnitPackages.belongsTo(models.HandlingUnit)
    HandlingUnitPackages.belongsTo(models.Package)
  }

  return HandlingUnitPackages
}
