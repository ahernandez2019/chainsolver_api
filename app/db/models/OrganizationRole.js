const organizationRole = (sequelize, DataTypes) => {
  const OrganizationRole = sequelize.define('OrganizationRole', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    label: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  OrganizationRole.associate = models => {
    OrganizationRole.hasMany(models.OrganizationMember)
    OrganizationRole.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    OrganizationRole.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return OrganizationRole
}

module.exports = organizationRole
