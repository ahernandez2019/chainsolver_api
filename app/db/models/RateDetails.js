const rateDetails = (sequelize, DataTypes) => {
  const RateDetails = sequelize.define('RateDetails', {
    validitySince: {
      type: DataTypes.DATE,
      required: true
    },
    validityUntil: {
      type: DataTypes.DATE,
      required: true
    },
    originPort: {
      type: DataTypes.STRING,
      required: true
    },
    freeTime: {
      type: DataTypes.INTEGER,
      required: true
    },
    isps: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })
  RateDetails.associate = models => {
  //  RateDetails.belongsTo(models.Port, { foreignKey: 'originPort', as: 'OriginPort' })
    RateDetails.belongsTo(models.Rate)
    RateDetails.hasMany(models.RatePorts)
    RateDetails.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    RateDetails.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }
  return RateDetails
}

module.exports = rateDetails
