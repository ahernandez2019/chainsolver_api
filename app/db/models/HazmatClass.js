'use strict'
module.exports = (sequelize, DataTypes) => {
  const HazmatClass = sequelize.define('HazmatClass', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  }, { timestamps: true })
  HazmatClass.associate = function (models) {
    // associations can be defined here
  }
  return HazmatClass
}
