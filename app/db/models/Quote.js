const quote = (sequelize, DataTypes) => {
  const Quote = sequelize.define('Quote', {
    value: {
      type: DataTypes.STRING,
      required: true
    },
    freedays: {
      type: DataTypes.INTEGER,
      required: true
    },
    destinationPort: {
      type: DataTypes.INTEGER,
      required: true
    },
    originPort: {
      type: DataTypes.INTEGER,
      required: true
    },
    status: {
      type: DataTypes.BOOLEAN,
      required: true
    },
    route: {
      type: DataTypes.STRING,
      required: true
    },
    validFrom: {
      type: DataTypes.DATE,
      required: true
    },
    validuntil: {
      type: DataTypes.DATE,
      required: true
    }
  })
  Quote.associate = models => {
    Quote.belongsTo(models.Commodity)
    Quote.belongsTo(models.ServiceType)
    Quote.belongsTo(models.User)
    Quote.belongsTo(models.Currency)
    Quote.belongsTo(models.ShippingLine)
    Quote.belongsTo(models.MultiplierFactor)
    Quote.belongsTo(models.Port, { foreignKey: 'originPort', as: 'OriginPort' })
    Quote.belongsTo(models.Port, { foreignKey: 'destinationPort', as: 'DestinationPort' })
  }
  return Quote
}

module.exports = quote
