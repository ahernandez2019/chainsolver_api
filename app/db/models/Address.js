const address = (sequelize, DataTypes) => {
  const Address = sequelize.define('Address', {
    address: {
      type: DataTypes.STRING,
      required: true
    },
    address2: DataTypes.STRING,

    zipCode: {
      type: DataTypes.STRING,
      required: true
    },
    lat: {
      type: DataTypes.STRING
    },
    long: {
      type: DataTypes.STRING
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Address.associate = models => {
    Address.belongsTo(models.Contact, { constraint: false })
    Address.belongsTo(models.City)
    Address.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Address.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Address
}

module.exports = address
