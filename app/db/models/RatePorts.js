const ratePorts = (sequelize, DataTypes) => {
  const RatePorts = sequelize.define('RatePorts', {
    destinationPort: {
      type: DataTypes.STRING,
      required: true
    },
    rateDetailsId: {
      type: DataTypes.INTEGER,
      required: true
    }
  })
  RatePorts.associate = models => {
    //  RatePorts.belongsTo(models.Port, { foreignKey: 'destinationPort', as: 'DestinationPort' })
    RatePorts.belongsTo(models.RateDetails, { foreignKey: 'rateDetailsId', as: 'RateDetailsId' })
    RatePorts.hasMany(models.ContainerRates)
  }
  return RatePorts
}

module.exports = ratePorts
