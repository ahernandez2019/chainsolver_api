const serviceType = (sequelize, DataTypes) => {
  const ServiceType = sequelize.define('ServiceType', {
    value: {
      type: DataTypes.STRING,
      required: true
    }
  })
  ServiceType.associate = models => {
    ServiceType.hasMany(models.Quote)
  }
  return ServiceType
}

module.exports = serviceType
