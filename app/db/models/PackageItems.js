'use strict'
module.exports = (sequelize, DataTypes) => {
  const PackageItems = sequelize.define('PackageItems', {
    quantity: {
      type: DataTypes.DECIMAL,
      required: true
    }
  }, { timestamps: true })
  PackageItems.associate = models => {
    PackageItems.belongsTo(models.Package)
    PackageItems.belongsTo(models.Item)
  }
  return PackageItems
}
