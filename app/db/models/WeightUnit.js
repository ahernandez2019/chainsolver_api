const weightUnit = (sequelize, DataTypes) => {
  const WeightUnit = sequelize.define('WeightUnit', {
    name: {
      type: DataTypes.STRING,
      required: true
    }
  })

  return WeightUnit
}

module.exports = weightUnit
