const multiplierFactor = (sequelize, DataTypes) => {
  const MultiplierFactor = sequelize.define('MultiplierFactor', {
    data: {
      type: DataTypes.STRING,
      required: true
    }
  })

  return MultiplierFactor
}

module.exports = multiplierFactor
