'use strict'
module.exports = (sequelize, DataTypes) => {
  const HazmatItems = sequelize.define('HazmatItems', {
    enable: {
      type: DataTypes.BOOLEAN,
      required: true
    }
  }, { timestamps: true })
  HazmatItems.associate = models => {
    HazmatItems.belongsTo(models.Item)
    HazmatItems.belongsTo(models.HazmatClass)
  }

  return HazmatItems
}
