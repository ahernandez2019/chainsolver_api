const shippingLine = (sequelize, DataTypes) => {
  const ShippingLine = sequelize.define('ShippingLine', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    description: {
      type: DataTypes.STRING,
      required: true
    },
    code: {
      type: DataTypes.STRING,
      required: true
    },
    status: {
      type: DataTypes.STRING,
      required: true
    }
  })
  return ShippingLine
}

module.exports = shippingLine
