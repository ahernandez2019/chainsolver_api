const contact = (sequelize, DataTypes) => {
  const Contact = sequelize.define('Contact', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    title: {
      type: DataTypes.STRING,
      required: true
    },
    email: {
      type: DataTypes.STRING,
      required: true,
      validate: {
        isEmail: {
          msg: 'The email contact is required'
        }
      }
    },
    gender: {
      type: DataTypes.ENUM('M', 'F')
    },
    birthday: {
      type: DataTypes.DATEONLY
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Contact.associate = models => {
    Contact.hasMany(models.Phone)
    Contact.hasMany(models.Address, { constraints: false })
    Contact.belongsTo(models.User, { constraints: false })
    Contact.belongsTo(models.Location, { constraints: false })
    Contact.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Contact.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Contact
}

module.exports = contact
