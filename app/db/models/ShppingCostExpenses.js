const shippingCostExpenses = (sequelize, DataTypes) => {
  const ShippingCostExpenses = sequelize.define('ShippingCostExpenses', {
    name: {
      type: DataTypes.STRING,
      required: true
    }
  })
  ShippingCostExpenses.associate = models => {
    ShippingCostExpenses.belongsTo(models.ShippingCost)
    ShippingCostExpenses.belongsTo(models.Expense)
  }
  return ShippingCostExpenses
}

module.exports = shippingCostExpenses
