const currency = (sequelize, DataTypes) => {
  const Currency = sequelize.define('Currency', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    label: {
      type: DataTypes.STRING,
      required: true
    },
    symbol: {
      type: DataTypes.STRING,
      required: true
    }
  })
  return Currency
}

module.exports = currency
