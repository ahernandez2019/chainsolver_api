const bcrypt = require('bcryptjs')

const user = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    temporaryToken: {
      type: DataTypes.TEXT
    },
    resetToken: {
      type: DataTypes.TEXT
    },
    isVerified: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    email: {
      type: DataTypes.STRING,
      required: true
    },
    password: {
      type: DataTypes.STRING,
      required: true
    }
  })
  User.associate = models => {
    User.belongsTo(models.Organization, { constraints: false })
  }
  User.prototype.comparePassword = function (candidatePassword, cb) {
    console.log(this.password, candidatePassword)
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
      if (err) {
        return cb(err)
      }
      cb(null, isMatch)
    })
  }
  User.beforeCreate(async function (user, options) {
    try {
      const hash = await cryptPassword(user.password)
      user.password = hash
      console.log('done')
    } catch (e) {
      console.error(e)
    }
  })
  User.beforeUpdate(async function (user, options) {
    try {
      const hash = await cryptPassword(user.password)
      user.password = hash
      console.log('done')
    } catch (e) {
      console.error(e)
    }
  })
  const cryptPassword = async function (password) {
    try {
      const salt = await bcrypt.genSalt(10)
      const hash = await bcrypt.hash(password, salt, null)
      return hash
    } catch (e) {
      console.error(e)
    }
  }
  return User
}

module.exports = user
