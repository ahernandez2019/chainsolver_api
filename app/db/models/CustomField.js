const CustomField = (sequelize, DataTypes) => {
  const CustomField = sequelize.define('CustomField', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    label: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  CustomField.associate = models => {
    CustomField.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    CustomField.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return CustomField
}

module.exports = CustomField
