const locationType = (sequelize, DataTypes) => {
  const LocationType = sequelize.define('LocationType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    label: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  LocationType.associate = models => {
    LocationType.hasMany(models.Location)
    LocationType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    LocationType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return LocationType
}

module.exports = locationType
