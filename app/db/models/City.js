const city = (sequelize, DataTypes) => {
  const City = sequelize.define('City', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    },
    flag: {
      type: DataTypes.BOOLEAN
    }
  })

  City.associate = models => {
    City.hasMany(models.Address)
    City.belongsTo(models.State)
    City.belongsTo(models.Country)
    City.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    City.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return City
}

module.exports = city
