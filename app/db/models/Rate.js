const rate = (sequelize, DataTypes) => {
  const Rate = sequelize.define('Rate', {
    countryOfOrigin: {
      type: DataTypes.STRING,
      required: true
    },
    countryOfDestiny: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })
  Rate.associate = models => {
    Rate.hasMany(models.RateDetails)
    //    Rate.belongsTo(models.Country, { foreignKey: 'countryOfOrigin', as: 'CountryOfOrigin' })
    //    Rate.belongsTo(models.Country, { foreignKey: 'countryOfDestiny', as: 'CountryOfDestiny' })
    Rate.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Rate.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }
  return Rate
}

module.exports = rate
