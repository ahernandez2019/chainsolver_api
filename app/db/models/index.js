'use strict'

// Libraries
const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const _ = require('lodash')

const basename = path.basename(__filename)
const env = process.env.NODE_ENV || 'development'
const config = require(path.join(__dirname, '../config/index.js'))[env]
const db = {}

const sequelize = new Sequelize(config.database, config.username, config.password, config)

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

sequelize.addHook('beforeCreate', model => {
  model.createdBy = 1
})

sequelize.addHook('beforeUpdate', model => {
  model.updatedBy = 1
})

const excludeModels = ['Country']

const addCreatedByData = model => {
  const modelName = _.get(model, '_modelOptions.name.singular', '--')

  if (excludeModels.includes(modelName)) {
    return true
  }

  model.createdBy = _.get(model, 'CreatedBy.name', '') + ' on ' + _.get(model, 'createdAt', '')
  model.createdBy = model.createdBy.replace('GMT+0000 (Coordinated Universal Time)', 'EST')
  const updatedName = _.get(model, 'UpdatedBy.name', '')
  if (updatedName !== '' && model.updatedBy) {
    model.updatedBy = updatedName + ' on ' + _.get(model, 'updatedAt', '')
    model.updatedBy = model.createdBy.replace('GMT+0000 (Coordinated Universal Time)', 'EST')
  }
  delete model.CreatedBy
}

sequelize.addHook('afterFind', record => {
  if (record === null) {
    return true
  }
  if (_.isArray(record)) {
    _.each(record, item => addCreatedByData(item))
  } else {
    addCreatedByData(record)
  }
})

module.exports = db
