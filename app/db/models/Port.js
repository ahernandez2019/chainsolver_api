const port = (sequelize, DataTypes) => {
  const Port = sequelize.define('Port', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Port.associate = models => {
    Port.hasOne(models.Address)
    Port.belongsTo(models.State)
    Port.belongsTo(models.City)
    Port.belongsTo(models.Country)
    Port.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Port.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Port
}

module.exports = port
