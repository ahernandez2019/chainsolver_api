const lengthUnit = (sequelize, DataTypes) => {
  const LengthUnit = sequelize.define('LengthUnit', {
    name: {
      type: DataTypes.STRING,
      required: true
    }
  })
  return LengthUnit
}

module.exports = lengthUnit
