const unitType = (sequelize, DataTypes) => {
  const UnitType = sequelize.define('UnitType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  UnitType.associate = models => {
    UnitType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    UnitType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return UnitType
}

module.exports = unitType
