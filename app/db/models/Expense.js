const expense = (sequelize, DataTypes) => {
  const Expense = sequelize.define('Expense', {
    value: {
      type: DataTypes.STRING,
      required: true
    }
  })
  return Expense
}

module.exports = expense
