const organizationMember = (sequelize, DataTypes) => {
  const OrganizationMember = sequelize.define('OrganizationMember', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  OrganizationMember.associate = models => {
    OrganizationMember.belongsTo(models.OrganizationRole)
    OrganizationMember.belongsTo(models.Organization)
    OrganizationMember.belongsTo(models.User)
    OrganizationMember.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    OrganizationMember.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return OrganizationMember
}

module.exports = organizationMember
