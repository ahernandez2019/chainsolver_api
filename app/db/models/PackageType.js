const packageType = (sequelize, DataTypes) => {
  const PackageType = sequelize.define('PackageType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  PackageType.associate = models => {
    PackageType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    PackageType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return PackageType
}

module.exports = packageType
