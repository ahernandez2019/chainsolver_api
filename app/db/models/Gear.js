'use strict'
module.exports = (sequelize, DataTypes) => {
  const Gear = sequelize.define('Gear', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  }, { timestamps: true })
  Gear.associate = (models) => {
    Gear.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Gear.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
    Gear.belongsTo(models.GearType)
  }
  return Gear
}
