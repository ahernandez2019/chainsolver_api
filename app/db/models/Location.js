const location = (sequelize, DataTypes) => {
  const Location = sequelize.define('Location', {
    isHq: {
      type: DataTypes.BOOLEAN
    },
    name: {
      type: DataTypes.STRING,
      required: true
    },
    loadingDock: {
      type: DataTypes.BOOLEAN,
      required: true
    },
    forklift: {
      type: DataTypes.BOOLEAN,
      required: true
    },
    helpLoading: {
      type: DataTypes.BOOLEAN,
      required: true
    },
    operationHours: {
      type: DataTypes.STRING,
      required: true
    },
    needsAppointment: {
      type: DataTypes.BOOLEAN,
      required: true
    },
    notes: {
      type: DataTypes.TEXT,
      required: false
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Location.associate = models => {
    Location.hasMany(models.Contact)
    Location.belongsTo(models.Address)
    Location.belongsTo(models.Phone)
    Location.belongsTo(models.Organization, { constraint: false })
    Location.belongsTo(models.LocationType, { constraint: false })
    Location.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Location.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Location
}

module.exports = location
