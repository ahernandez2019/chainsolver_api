'use strict'
module.exports = (sequelize, DataTypes) => {
  const GearType = sequelize.define('GearType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  }, { timestamps: true })
  GearType.associate = (models) => {
    GearType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    GearType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
    GearType.hasMany(models.Gear)
  }
  return GearType
}
