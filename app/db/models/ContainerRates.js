const containerRates = (sequelize, DataTypes) => {
  const ContainerRates = sequelize.define('ContainerRates', {
    ContainerType: {
      type: DataTypes.INTEGER,
      required: true
    },
    Value: {
      type: DataTypes.FLOAT,
      required: true
    },
    Currency: {
      type: DataTypes.INTEGER,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  ContainerRates.associate = models => {
    ContainerRates.belongsTo(models.RatePorts)
    ContainerRates.belongsTo(models.ContainerType, { foreignKey: 'ContainerType', as: 'containerType' })
    ContainerRates.belongsTo(models.Currency, { foreignKey: 'Currency', as: 'currency' })
    ContainerRates.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    ContainerRates.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return ContainerRates
}

module.exports = containerRates
