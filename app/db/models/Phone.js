const phone = (sequelize, DataTypes) => {
  const Phone = sequelize.define('Phone', {
    number: {
      type: DataTypes.STRING,
      required: true
    },
    fax: {
      type: DataTypes.STRING
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Phone.associate = models => {
    Phone.belongsTo(models.Contact, { constraints: false })
    Phone.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Phone.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Phone
}

module.exports = phone
