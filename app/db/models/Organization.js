const organization = (sequelize, DataTypes) => {
  const Organization = sequelize.define('Organization', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    website: {
      type: DataTypes.STRING
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    },
    bankAccName: {
      type: DataTypes.STRING
    },
    bankAccNumber: {
      type: DataTypes.STRING
    },
    bankName: {
      type: DataTypes.STRING
    },
    bankAbn: {
      type: DataTypes.STRING
    },
    bankSwift: {
      type: DataTypes.STRING
    },
    bankAddress: {
      type: DataTypes.STRING
    },
    bankCity: {
      type: DataTypes.STRING
    },
    bankState: {
      type: DataTypes.STRING
    },
    bankZip: {
      type: DataTypes.STRING
    },
    bankCountry: {
      type: DataTypes.STRING
    },
    billingAddress: {
      type: DataTypes.STRING
    },
    billingCity: {
      type: DataTypes.STRING
    },
    billingState: {
      type: DataTypes.STRING
    },
    billingZip: {
      type: DataTypes.STRING
    },
    billingCountry: {
      type: DataTypes.STRING
    },
    billingPhone: {
      type: DataTypes.STRING
    },
    billingFax: {
      type: DataTypes.STRING
    },
    billingEmail: {
      type: DataTypes.STRING
    },
    billingNotes: {
      type: DataTypes.STRING
    }
  })

  Organization.associate = models => {
    Organization.hasMany(models.Location)
    Organization.hasMany(models.OrganizationMember)
    Organization.belongsTo(models.OrganizationType)
    Organization.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Organization.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Organization
}

module.exports = organization
