const shippingCost = (sequelize, DataTypes) => {
  const ShippingCost = sequelize.define('ShippingCost', {
    type: {
      type: DataTypes.ENUM({
        values: ['Origin', 'Destination']
      }),
      required: true
    },
    pickUp: {
      type: DataTypes.STRING
    },
    notes: {
      type: DataTypes.TEXT
    },
    customTax: {
      type: DataTypes.FLOAT
    },
    portCharges: {
      type: DataTypes.FLOAT
    },
    thc: {
      type: DataTypes.STRING
    },
    totalValue: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Expenses || !this.Expenses.length) return 0
        return this.Expenses.reduce((accumulator, expense) => { return accumulator + (expense.value ? expense.value : 0) }, (this.customTax + this.portCharges) || 0)
      }
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  ShippingCost.associate = models => {
    ShippingCost.belongsTo(models.Address)
    ShippingCost.belongsTo(models.Currency)
    ShippingCost.belongsToMany(models.Expense, { through: 'ShippingCostExpenses', as: 'Expenses' })
    ShippingCost.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    ShippingCost.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return ShippingCost
}

module.exports = shippingCost
