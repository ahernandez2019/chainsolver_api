const agentProfit = (sequelize, DataTypes) => {
  const AgentProfit = sequelize.define('AgentProfit', {
    profit: {
      type: DataTypes.FLOAT,
      required: true
    },
    messaging: {
      type: DataTypes.FLOAT,
      required: true
    },
    totalValue: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Expenses || !this.Expenses.length) return 0
        return this.Expenses.reduce((accumulator, expense) => { return accumulator + (expense.value ? expense.value : 0) }, (this.profit + this.messaging) || 0)
      }
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  AgentProfit.associate = models => {
    AgentProfit.belongsToMany(models.Expense, { through: 'AgentProfitExpenses', as: 'Expenses' })
    AgentProfit.belongsTo(models.Quote)
    AgentProfit.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    AgentProfit.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return AgentProfit
}

module.exports = agentProfit
