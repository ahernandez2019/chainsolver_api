'use strict'
module.exports = (sequelize, DataTypes) => {
  const CustomFieldItems = sequelize.define('CustomFieldItems', {
    value: {
      type: DataTypes.STRING,
      required: true
    }
  }, { timestamps: true })
  CustomFieldItems.associate = models => {
    CustomFieldItems.belongsTo(models.CustomField)
    CustomFieldItems.belongsTo(models.Item)
  }
  return CustomFieldItems
}
