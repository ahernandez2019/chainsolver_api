const organizationType = (sequelize, DataTypes) => {
  const OrganizationType = sequelize.define('OrganizationType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  OrganizationType.associate = models => {
    OrganizationType.hasMany(models.Organization)
    OrganizationType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    OrganizationType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return OrganizationType
}

module.exports = organizationType
