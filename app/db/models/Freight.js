const freight = (sequelize, DataTypes) => {
  const Freight = sequelize.define('Freight', {
    value: {
      type: DataTypes.FLOAT,
      required: true
    },
    bunkerBaf: {
      type: DataTypes.STRING
    },
    totalValue: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Expenses || !this.Expenses.length) return 0
        return this.Expenses.reduce((accumulator, expense) => { return accumulator + (expense.value ? expense.value : 0) }, this.value || 0)
      }
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  Freight.associate = models => {
    Freight.belongsToMany(models.Expense, { through: 'FreightExpenses', as: 'Expenses' })
    //  Freight.hasOne(models.Container)
    Freight.belongsTo(models.Quote)
    Freight.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Freight.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return Freight
}

module.exports = freight
