const state = (sequelize, DataTypes) => {
  const State = sequelize.define('State', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    flag: {
      type: DataTypes.BOOLEAN
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  State.associate = models => {
    State.hasMany(models.City)
    State.belongsTo(models.Country)
    State.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    State.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return State
}

module.exports = state
