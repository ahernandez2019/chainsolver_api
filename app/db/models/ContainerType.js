const containerType = (sequelize, DataTypes) => {
  const ContainerType = sequelize.define('ContainerType', {
    size: {
      type: DataTypes.ENUM({
        values: ['20', '40']
      }),
      required: true
    },
    type: {
      type: DataTypes.ENUM({
        values: ['ST', 'HQ', 'NOR']
      }),
      required: true
    }
  })

  return ContainerType
}

module.exports = containerType
