const commodity = (sequelize, DataTypes) => {
  const Commodity = sequelize.define('Commodity', {
    value: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })
  Commodity.associate = models => {
    Commodity.hasMany(models.Quote)
    Commodity.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Commodity.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }
  return Commodity
}

module.exports = commodity
