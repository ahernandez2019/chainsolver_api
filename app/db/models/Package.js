const convert = require('convert-units')
const Package = (sequelize, DataTypes) => {
  const Package = sequelize.define('Package', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    unitLength: {
      type: DataTypes.FLOAT,
      required: true
    },
    unitWidth: {
      type: DataTypes.FLOAT,
      required: true
    },
    unitHeight: {
      type: DataTypes.FLOAT,
      required: true
    },
    packagePhoto: {
      type: DataTypes.STRING,
      required: true
    },
    emptyPackageWeight: {
      type: DataTypes.FLOAT,
      required: true
    },
    notes: {
      type: DataTypes.TEXT,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    },
    lengthUnit: {
      type: DataTypes.INTEGER,
      required: true
    },
    weightUnit: {
      type: DataTypes.INTEGER,
      required: true
    },
    currency: {
      type: DataTypes.INTEGER,
      required: true
    },
    unitVolume: {
      type: DataTypes.VIRTUAL,
      get () {
        return this.getDataValue('unitLength') * this.getDataValue('unitWidth') * this.getDataValue('unitHeight')
      }
    },
    countriesOfOrigin: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Items || !this.Items.length) return
        return this.Items.map(item => item.Country.name).filter((value, index, array) => array.indexOf(value) === index)
      }
    },
    weight: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Items || !this.Items.length) return this.getDataValue('emptyPackageWeight')
        return this.Items.reduce(
          (accumulator, item) => {
            let weight = item.unitWeight
            if (this.WeightUnit.name !== item.WeightUnit.name) {
              let from = item.WeightUnit.name
              let to = this.WeightUnit.name
              weight = convert(item.unitWeight).from(from).to(to)
            }
            return accumulator + Number(weight) * (item.getDataValue('PackageItems') ? Number(item.getDataValue('PackageItems').quantity) : 0)
          }, 0) + this.getDataValue('emptyPackageWeight')
      }
    },
    unitValue: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Items || !this.Items.length) return 0
        return this.Items.reduce((accumulator, item) => { return accumulator + item.unitValue * (item.getDataValue('PackageItems') ? Number(item.getDataValue('PackageItems').quantity) : 0) }, 0)
      }
    },
    hazmatClasses: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Items || !this.Items.length) return
        const hazmatClassesMatrix = this.Items.map(item => item.HazmatClasses.map(hazmatClass => hazmatClass.name)).filter((value, index, array) => array.indexOf(value) === index)
        const hazmatClassesArray = hazmatClassesMatrix.reduce((accumulator, item) => { return accumulator.concat(item) }, [])
        return hazmatClassesArray.filter((value, index, array) => array.indexOf(value) === index)
      }
    }
  }, { timestamps: true })
  Package.associate = models => {
    Package.belongsTo(models.Currency, { foreignKey: 'currency', as: 'Currency' })
    Package.belongsTo(models.LengthUnit, { foreignKey: 'lengthUnit', as: 'LengthUnit' })
    Package.belongsTo(models.WeightUnit, { foreignKey: 'weightUnit', as: 'WeightUnit' })
    Package.belongsTo(models.PackageType)
    Package.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    Package.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
    Package.belongsToMany(models.HandlingUnit, { through: 'HandlingUnitPackages', as: 'HandlingUnits' })
    Package.belongsToMany(models.Item, { through: 'PackageItems', as: 'Items' })
  }
  return Package
}

module.exports = Package
