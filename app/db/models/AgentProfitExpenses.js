const agentProfitExpenses = (sequelize, DataTypes) => {
  const AgentProfitExpenses = sequelize.define('AgentProfitExpenses', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })
  AgentProfitExpenses.associate = models => {
    AgentProfitExpenses.belongsTo(models.AgentProfit)
    AgentProfitExpenses.belongsTo(models.Expense)
    AgentProfitExpenses.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    AgentProfitExpenses.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }
  return AgentProfitExpenses
}

module.exports = agentProfitExpenses
