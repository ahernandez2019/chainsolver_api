const itemType = (sequelize, DataTypes) => {
  const ItemType = sequelize.define('ItemType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  ItemType.associate = models => {
    ItemType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    ItemType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return ItemType
}

module.exports = itemType
