const freightExpenses = (sequelize, DataTypes) => {
  const FreightExpenses = sequelize.define('FreightExpenses', {
    name: {
      type: DataTypes.STRING,
      required: true
    }
  })
  FreightExpenses.associate = models => {
    FreightExpenses.belongsTo(models.Freight)
    FreightExpenses.belongsTo(models.Expense)
  }
  return FreightExpenses
}

module.exports = freightExpenses
