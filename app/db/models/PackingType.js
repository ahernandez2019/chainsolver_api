const packingType = (sequelize, DataTypes) => {
  const PackingType = sequelize.define('PackingType', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  PackingType.associate = models => {
    PackingType.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    PackingType.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return PackingType
}

module.exports = packingType
