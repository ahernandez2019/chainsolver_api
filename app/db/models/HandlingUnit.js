const convert = require('convert-units')
module.exports = (sequelize, DataTypes) => {
  const HandlingUnit = sequelize.define('HandlingUnit', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    hazmatClasses: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Packages || !this.Packages.length) return
        const hazmatClassesMatrix = this.Packages.map(pack => pack.hazmatClasses && pack.hazmatClasses.length ? pack.hazmatClasses.map(hazmatClass => hazmatClass) : []).filter((value, index, array) => array.indexOf(value) === index)
        const hazmatClassesArray = hazmatClassesMatrix.reduce((accumulator, item) => { return accumulator.concat(item) }, [])
        return hazmatClassesArray.filter((value, index, array) => array.indexOf(value) === index)
      }
    },
    instructions: {
      type: DataTypes.TEXT
    },
    unitLength: {
      type: DataTypes.FLOAT,
      required: true,
      get: function () {
        return this.getDataValue('unitLength')
      }
    },
    weight: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Packages || !this.Packages.length) return this.getDataValue('emptyUnitWeight')
        return this.Packages.reduce(
          (accumulator, Package) => {
            let weight = Package.weight
            if (this.WeightUnit.name !== Package.WeightUnit.name) {
              let from = Package.WeightUnit.name
              let to = this.WeightUnit.name
              weight = convert(Package.weight).from(from).to(to)
            }
            return accumulator + Number(weight) * (Package.getDataValue('HandlingUnitPackages') ? Number(Package.getDataValue('HandlingUnitPackages').quantity) : 0)
          }, 0) + this.getDataValue('emptyUnitWeight')
      }
    },
    unitWidth: {
      type: DataTypes.FLOAT,
      required: true,
      get: function () {
        return this.getDataValue('unitWidth')
      }
    },
    unitHeight: {
      type: DataTypes.FLOAT,
      required: true,
      get: function () {
        return this.getDataValue('unitHeight')
      }
    },
    emptyUnitWeight: {
      type: DataTypes.FLOAT,
      required: true,
      get: function () {
        return this.getDataValue('emptyUnitWeight')
      }
    },
    lengthUnit: {
      type: DataTypes.INTEGER,
      required: true
    },
    weightUnit: {
      type: DataTypes.INTEGER,
      required: true
    },
    currency: {
      type: DataTypes.INTEGER,
      required: true
    },
    unitVolume: {
      type: DataTypes.VIRTUAL,
      get: function () {
        return this.get('unitWidth') * this.get('unitHeight') * this.get('unitLength')
      }
    },
    unitValue: {
      type: DataTypes.VIRTUAL,
      get () {
        if (!this.Packages || !this.Packages.length) return 0
        return this.Packages.reduce((accumulator, item) => {
          return accumulator + item.unitValue * (item.getDataValue('HandlingUnitPackages') ? Number(item.getDataValue('HandlingUnitPackages').quantity) : 0)
        }, 0)
      }
    },
    unitPicture: {
      type: DataTypes.STRING
    },
    unitDiagram: {
      type: DataTypes.STRING
    },
    unitDimensions: {
      type: DataTypes.TEXT,
      required: true
    },
    notes: {
      type: DataTypes.TEXT,
      required: false
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  }, { timestamps: true }
  )

  HandlingUnit.associate = models => {
    HandlingUnit.belongsTo(models.Currency, { foreignKey: 'currency', as: 'Currency' })
    HandlingUnit.belongsTo(models.Country)
    HandlingUnit.belongsTo(models.UnitType)
    HandlingUnit.belongsTo(models.LengthUnit, { foreignKey: 'lengthUnit', as: 'LengthUnit' })
    HandlingUnit.belongsTo(models.WeightUnit, { foreignKey: 'weightUnit', as: 'WeightUnit' })
    HandlingUnit.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    HandlingUnit.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
    HandlingUnit.belongsToMany(models.Package, { through: 'HandlingUnitPackages', as: 'Packages' })
  }

  return HandlingUnit
}
