const itemCategory = (sequelize, DataTypes) => {
  const ItemCategory = sequelize.define('ItemCategory', {
    name: {
      type: DataTypes.STRING,
      required: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  })

  ItemCategory.associate = models => {
    ItemCategory.belongsTo(models.User, { foreignKey: 'CreatedBy', as: 'createdBy' })
    ItemCategory.belongsTo(models.User, { foreignKey: 'UpdatedBy', as: 'updatedBy' })
  }

  return ItemCategory
}

module.exports = itemCategory
