const Item = (sequelize, DataTypes) => {
  const Item = sequelize.define('Item', {
    name: {
      type: DataTypes.STRING,
      required: true,
      validate: {
        notEmpty: true
      }
    },
    subItemQuantity: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pictureOfProduct: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    hsCode: {
      type: DataTypes.STRING
    },
    naicsCode: {
      type: DataTypes.STRING
    },
    skuNumber: {
      type: DataTypes.STRING
    },
    upcCode: {
      type: DataTypes.STRING
    },
    isbnCode: {
      type: DataTypes.STRING
    },
    barCode: {
      type: DataTypes.STRING
    },
    mpnCode: {
      type: DataTypes.STRING
    },
    unitLength: {
      type: DataTypes.FLOAT,
      required: true,
      validate: {
        notEmpty: true
      },
      get () {
        return this.getDataValue('unitLength')
      }
    },
    unitWidth: {
      type: DataTypes.FLOAT,
      required: true,
      validate: {
        notEmpty: true
      },
      get () {
        return this.getDataValue('unitWidth')
      }
    },
    unitHeight: {
      type: DataTypes.FLOAT,
      required: true,
      validate: {
        notEmpty: true
      },
      get () {
        return this.getDataValue('unitHeight')
      }
    },
    unitWeight: {
      type: DataTypes.FLOAT,
      get () {
        return this.getDataValue('unitWeight')
      },
      required: true,
      validate: {
        notEmpty: true
      }
    },
    unitVolume: {
      type: DataTypes.VIRTUAL,
      get () {
        return this.getDataValue('unitLength') * this.getDataValue('unitWidth') * this.getDataValue('unitHeight')
      }
    },
    eanCode: {
      type: DataTypes.STRING
    },
    unitValue: {
      type: DataTypes.FLOAT,
      get () {
        return this.getDataValue('unitValue')
      },
      required: true,
      validate: {
        notEmpty: true
      }
    },
    parentId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    version: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lengthUnit: {
      type: DataTypes.INTEGER,
      required: true
    },
    weightUnit: {
      type: DataTypes.INTEGER,
      required: true
    },
    currency: {
      type: DataTypes.INTEGER,
      required: true
    },
    supplier: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    manufacturer: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    CreatedBy: {
      type: DataTypes.INTEGER
    },
    UpdatedBy: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: true
  })
  Item.associate = models => {
    Item.belongsTo(models.LengthUnit, { foreignKey: 'lengthUnit', as: 'LengthUnit' })
    Item.belongsTo(models.WeightUnit, { foreignKey: 'weightUnit', as: 'WeightUnit' })
    Item.belongsTo(models.Country)
    Item.belongsTo(models.ItemCategory)
    Item.belongsTo(models.PackageType)
    Item.belongsTo(models.Currency, { foreignKey: 'currency', as: 'Currency' })
    Item.belongsToMany(models.HazmatClass, {
      through: 'HazmatItems',
      as: 'HazmatClasses'
    })
    Item.belongsTo(models.Organization, {
      foreignKey: {
        name: 'supplier'
      },
      as: 'Supplier',
      constraint: false
    })
    Item.belongsTo(models.Organization)
    Item.belongsTo(models.Organization, {
      foreignKey: {
        name: 'manufacturer'
      },
      as: 'Manufacturer'
    })
    Item.belongsTo(models.Location, {
      foreignKey: {
        name: 'manufacturerLocationId'
      },
      as: 'ManufacturerLocationId'
    })
    Item.belongsTo(models.PackingType)
    Item.belongsTo(models.User, {
      foreignKey: 'CreatedBy',
      as: 'createdBy'
    })
    Item.belongsTo(models.User, {
      foreignKey: 'UpdatedBy',
      as: 'updatedBy'
    })
    Item.belongsTo(models.Item, { // version parent
      foreignKey: 'parentId',
      constraint: false
    })
    Item.belongsToMany(models.CustomField, {
      through: 'CustomFieldItems',
      as: 'CustomFields'
    })
  }

  return Item
}
module.exports = Item
