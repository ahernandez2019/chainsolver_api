'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Items', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      subItemQuantity: {
        type: Sequelize.INTEGER
      },
      pictureOfProduct: {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      notes: {
        type: Sequelize.TEXT
      },
      hsCode: {
        type: Sequelize.STRING
      },
      naicsCode: {
        type: Sequelize.STRING
      },
      skuNumber: {
        type: Sequelize.STRING
      },
      upcCode: {
        type: Sequelize.STRING
      },
      isbnCode: {
        type: Sequelize.STRING
      },
      barCode: {
        type: Sequelize.STRING
      },
      mpnCode: {
        type: Sequelize.STRING
      },
      unitLength: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      weightUnit: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'WeightUnits',
          key: 'id'
        },
        allowNull: false
      },
      lengthUnit: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'LengthUnits',
          key: 'id'
        },
        allowNull: false
      },
      unitWidth: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitHeight: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitWeight: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitValue: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      eanCode: {
        type: Sequelize.STRING
      },
      PackingTypeId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        allowNull: false,
        references: {
          model: 'PackingTypes',
          key: 'id'
        }
      },
      supplier: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Organizations',
          key: 'id'
        }
      },
      manufacturer: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Organizations',
          key: 'id'
        }
      },
      manufacturerLocationId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Locations',
          key: 'id'
        }
      },
      OrganizationId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Organizations',
          key: 'id'
        }
      },
      createdBy: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      CountryId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        allowNull: false,
        references: {
          model: 'Countries',
          key: 'id'
        }
      },
      currency: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'Currencies',
          key: 'id'
        }
      },
      ItemCategoryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'ItemCategories',
          key: 'id'
        }
      },
      PackageTypeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'PackageTypes',
          key: 'id'
        }
      },
      parentId: {
        type: Sequelize.INTEGER
      },
      version: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Items')
  }
}
