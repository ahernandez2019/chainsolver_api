'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Users', 'OrganizationId', {
      type: Sequelize.INTEGER,
      allowNull: true,
      constraint: false,
      references: {
        model: 'Organizations',
        key: 'id'
      }

    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Users',
      'OrganizationId'
    )
  }
}
