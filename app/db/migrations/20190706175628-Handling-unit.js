'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HandlingUnits', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      instructions: {
        type: Sequelize.TEXT
      },
      UnitTypeId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'UnitTypes',
          key: 'id'
        }
      },
      weightUnit: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'WeightUnits',
          key: 'id'
        },
        allowNull: false
      },
      lengthUnit: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'LengthUnits',
          key: 'id'
        },
        allowNull: false
      },
      currency: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'Currencies',
          key: 'id'
        }
      },
      unitLength: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitWidth: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitHeight: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      emptyUnitWeight: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitPicture: {
        type: Sequelize.STRING
      },
      unitDiagram: {
        type: Sequelize.STRING
      },
      unitDimensions: {
        type: Sequelize.TEXT
      },
      CountryId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Countries',
          key: 'id'
        }
      },
      notes: {
        type: Sequelize.TEXT
      },
      createdBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HandlingUnits')
  }
}
