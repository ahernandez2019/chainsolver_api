'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Cities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        eferences: {
          model: 'Users',
          key: 'id'
        }
      },
      flag: {
        type: Sequelize.BOOLEAN
      },
      CountryId: {
        type: Sequelize.INTEGER,
        eferences: {
          model: 'Countries',
          key: 'id'
        }
      },
      StateId: {
        type: Sequelize.INTEGER,
        eferences: {
          model: 'States',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Cities')
  }
}
