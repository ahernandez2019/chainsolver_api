'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HandlingUnitPackages', {
      HandlingUnitId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'HandlingUnits',
          key: 'id'
        }
      },
      PackageId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'Packages',
          key: 'id'
        }
      },
      quantity: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HandlingUnitPackages')
  }
}
