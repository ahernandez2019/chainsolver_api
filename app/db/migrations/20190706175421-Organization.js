'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Organizations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      website: {
        type: Sequelize.STRING
      },
      bankAccName: {
        type: Sequelize.STRING
      },
      bankAccNumber: {
        type: Sequelize.STRING
      },
      bankName: {
        type: Sequelize.STRING
      },
      bankAbn: {
        type: Sequelize.STRING
      },
      bankSwift: {
        type: Sequelize.STRING
      },
      bankAddress: {
        type: Sequelize.STRING
      },
      bankCity: {
        type: Sequelize.STRING
      },
      bankState: {
        type: Sequelize.STRING
      },
      bankZip: {
        type: Sequelize.STRING
      },
      bankCountry: {
        type: Sequelize.STRING
      },
      billingAddress: {
        type: Sequelize.STRING
      },
      billingCity: {
        type: Sequelize.STRING
      },
      billingState: {
        type: Sequelize.STRING
      },
      billingZip: {
        type: Sequelize.STRING
      },
      billingCountry: {
        type: Sequelize.STRING
      },
      billingPhone: {
        type: Sequelize.STRING
      },
      billingFax: {
        type: Sequelize.STRING
      },
      billingEmail: {
        type: Sequelize.STRING
      },
      billingNotes: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      OrganizationTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'OrganizationTypes',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Organizations')
  }
}
