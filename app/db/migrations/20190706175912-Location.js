'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Locations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      isHq: {
        type: Sequelize.BOOLEAN
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      loadingDock: {
        type: Sequelize.BOOLEAN
      },
      forklift: {
        type: Sequelize.BOOLEAN
      },
      helpLoading: {
        type: Sequelize.BOOLEAN
      },
      operationHours: {
        type: Sequelize.STRING,
        allowNull: false
      },
      needsAppointment: {
        type: Sequelize.BOOLEAN
      },
      notes: {
        type: Sequelize.TEXT
      },
      createdBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      AddressId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Addresses',
          key: 'id'
        }
      },
      PhoneId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'Phones',
          key: 'id'
        }
      },
      LocationTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'LocationTypes',
          key: 'id'
        }
      },
      OrganizationId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'Organizations',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Locations')
  }
}
