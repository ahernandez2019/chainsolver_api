'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PackageItems', {
      PackageId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'Packages',
          key: 'id'
        }
      },
      ItemId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'Items',
          key: 'id'
        }
      },
      quantity: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PackageItems')
  }
}
