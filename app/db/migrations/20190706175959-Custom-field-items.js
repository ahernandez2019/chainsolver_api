'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CustomFieldItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      CustomFieldId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'CustomFields',
          key: 'id'
        }
      },
      ItemId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'Items',
          key: 'id'
        }
      },
      value: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CustomFieldItems')
  }
}
