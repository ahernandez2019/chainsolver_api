'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Packages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      PackageTypeId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'PackageTypes',
          key: 'id'
        },
        allowNull: false
      },
      unitLength: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitWidth: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      unitHeight: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      packagePhoto: {
        type: Sequelize.STRING,
        allowNull: false
      },
      emptyPackageWeight: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      notes: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false
      },
      weightUnit: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'WeightUnits',
          key: 'id'
        },
        allowNull: false
      },
      currency: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'Currencies',
          key: 'id'
        }
      },
      lengthUnit: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'LengthUnits',
          key: 'id'
        },
        allowNull: false
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: false
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Packages')
  }
}
