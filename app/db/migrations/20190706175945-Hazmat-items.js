'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HazmatItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ItemId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'Items',
          key: 'id'
        }
      },
      enable: {
        type: Sequelize.BOOLEAN
      },
      HazmatClassId: {
        type: Sequelize.INTEGER,
        foreignKey: true,
        references: {
          model: 'HazmatClasses',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HazmatItems')
  }
}
