const env = require('../../../env')

const pgCreds = {
  username: env.db.dbUser,
  password: env.db.dbPassword,
  database: env.db.db,
  host: env.db.dbHost,
  port: env.db.dbPort,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    acquire: 1000,
    idle: 1000
  },
  logging: false
}

const config = {
  development: {
    ...pgCreds,
    logging: true
  },
  staging: {
    ...pgCreds,
    logging: true
  },
  production: {
    ...pgCreds
  }
}

module.exports = config
