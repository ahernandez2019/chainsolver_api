'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const name = faker.name.findName()
    const email = faker.internet.email()
    await queryInterface.bulkInsert('Users', [
      {
        name,
        email,
        password: faker.internet.password(),
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      }
    ], {})
    const users = await queryInterface.sequelize.query(
      `SELECT id from Users WHERE name="${name}" AND email="${email}";`
    )
    const userRows = users[0]
    const Admins = [{
      UserId: userRows[0].id,
      updatedBy: 1,
      createdBy: 1,
      CityId: faker.random.number({ min: 1, max: 22668 }),
      createdAt: faker.date.recent(2),
      updatedAt: faker.date.recent(2)
    }]
    await queryInterface.bulkInsert('Admins', Admins, {})
  },
  down: async function (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {})
    await queryInterface.bulkDelete('Admins', null, {})
  }
}
