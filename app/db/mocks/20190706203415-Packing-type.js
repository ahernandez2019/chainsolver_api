'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const PackingTypes = []
    for (var i = 1; i <= 3; i++) {
      PackingTypes.push({
        name: faker.commerce.productName(),
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2),
        createdBy: faker.random.number({ min: 1, max: 3 }),
        updatedBy: faker.random.number({ min: 1, max: 3 })
      })
    }
    return queryInterface.bulkInsert('PackingTypes', PackingTypes, {})
  },

  down: async function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('PackingTypes', null, {})
  }
}
