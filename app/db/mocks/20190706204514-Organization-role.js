'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const OrganizationRoles = []
    for (var i = 1; i <= 3; i++) {
      OrganizationRoles.push({
        name: faker.company.companyName(),
        label: faker.company.companySuffix(),
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('OrganizationRoles', OrganizationRoles, {})
  },

  down: async function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('OrganizationRoles', null, {})
  }
}
