'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const UnitTypes = []
    for (var i = 1; i <= 3; i++) {
      UnitTypes.push({
        name: faker.commerce.productName(),
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2),
        createdBy: faker.random.number({ min: 1, max: 3 }),
        updatedBy: faker.random.number({ min: 1, max: 3 })
      })
    }
    return queryInterface.bulkInsert('UnitTypes', UnitTypes, {})
  },

  down: async function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UnitTypes', null, {})
  }
}
