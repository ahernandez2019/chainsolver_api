'use strict'
const faker = require('faker')
module.exports = {
  up: (queryInterface, Sequelize) => {
    const Addresses = []
    for (var i = 1; i <= 3; i++) {
      Addresses.push({
        address: faker.address.secondaryAddress(),
        address2: faker.address.secondaryAddress(),
        zipCode: faker.address.zipCode(),
        coordinates: `${faker.address.latitude()}|${faker.address.longitude()}`,
        ContactId: 1,
        CityId: faker.random.number({ min: 1, max: 2200 }),
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('Addresses', Addresses, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Addresses', null, {})
  }
}
