'use strict'
const faker = require('faker')
module.exports = {
  up: (queryInterface, Sequelize) => {
    const Locations = []
    for (var i = 1; i <= 3; i++) {
      Locations.push({
        isHq: true,
        name: faker.address.streetAddress(),
        loadingDock: faker.random.boolean(),
        forklift: faker.random.boolean(),
        OrganizationId: 1,
        helpLoading: faker.random.boolean(),
        operationHours: `${faker.random.number({ min: 0, max: 11 })} - ${faker.random.number({ min: 12, max: 23 })}`,
        needsAppointment: faker.random.boolean(),
        notes: faker.lorem.paragraph(),
        AddressId: 1,
        PhoneId: 1,
        LocationTypeId: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('Locations', Locations, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Locations', null, {})
  }
}
