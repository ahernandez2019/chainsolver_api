'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const Contacts = []
    for (var i = 1; i <= 3; i++) {
      Contacts.push({
        name: faker.name.findName(),
        title: faker.name.jobTitle(),
        email: faker.internet.email(),
        gender: faker.random.boolean() ? 'M' : 'F',
        birthday: faker.date.past(),
        UserId: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    await queryInterface.bulkInsert('Contacts', Contacts, {})
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Contacts', null, {})
  }
}
