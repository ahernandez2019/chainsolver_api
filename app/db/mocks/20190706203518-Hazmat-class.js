'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const hazmatClasses = []
    for (var i = 1; i <= 3; i++) {
      hazmatClasses.push({
        name: faker.lorem.word(),
        createdBy: faker.random.number({ min: 1, max: 3 }),
        updatedBy: faker.random.number({ min: 1, max: 3 }),
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('HazmatClasses', hazmatClasses, {})
  },

  down: async function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('HazmatClasses', null, {})
  }
}
