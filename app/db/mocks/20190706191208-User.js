'use strict'
// const _ = require('lodash')
const faker = require('faker')

module.exports = {
  up: async function (queryInterface) {
    const Users = []
    for (var i = 0; i <= 2; i++) {
      Users.push({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    await queryInterface.bulkInsert('Users', Users, {})
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {})
  }
}
