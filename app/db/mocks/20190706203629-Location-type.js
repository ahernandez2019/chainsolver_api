'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const LocationTypes = []
    for (var i = 1; i <= 3; i++) {
      LocationTypes.push({
        name: faker.address.streetName(),
        label: faker.address.streetName(),
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('LocationTypes', LocationTypes, {})
  },

  down: async function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('LocationTypes', null, {})
  }
}
