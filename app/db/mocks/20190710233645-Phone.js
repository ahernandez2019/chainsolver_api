'use strict'
const faker = require('faker')
module.exports = {
  up: (queryInterface, Sequelize) => {
    const Phones = []
    for (var i = 1; i <= 3; i++) {
      Phones.push({
        number: faker.phone.phoneNumber(),
        fax: faker.phone.phoneNumber(),
        ContactId: faker.random.number({ min: 1, max: 3 }),
        createdBy: faker.random.number({ min: 1, max: 3 }),
        updatedBy: faker.random.number({ min: 1, max: 3 }),
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('Phones', Phones, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Phones', null, {})
  }
}
