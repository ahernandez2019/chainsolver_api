'use strict'
const faker = require('faker')
module.exports = {
  up: (queryInterface, Sequelize) => {
    const Organizations = []
    for (var i = 1; i <= 3; i++) {
      Organizations.push({
        name: faker.company.companyName(),
        website: faker.internet.url(),
        bankAccName: faker.finance.accountName(),
        bankAccNumber: faker.finance.account(),
        bankName: faker.company.companyName(),
        bankAbn: faker.finance.iban(),
        bankSwift: faker.finance.bic(),
        bankAddress: faker.address.streetAddress(),
        bankCity: faker.address.city(),
        bankState: faker.address.state(),
        bankZip: faker.address.zipCode(),
        bankCountry: faker.address.country(),
        billingAddress: faker.address.streetAddress(),
        billingCity: faker.address.city(),
        billingState: faker.address.state(),
        billingZip: faker.address.zipCode(),
        billingCountry: faker.address.country(),
        billingPhone: faker.phone.phoneNumber(),
        billingFax: faker.phone.phoneNumber(),
        billingEmail: faker.internet.email(),
        billingNotes: faker.lorem.words(),
        OrganizationTypeId: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('Organizations', Organizations, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Organizations', null, {})
  }
}
