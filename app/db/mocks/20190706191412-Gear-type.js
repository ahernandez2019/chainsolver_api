'use strict'
const faker = require('faker')
module.exports = {
  up: async function (queryInterface, Sequelize) {
    const records = []
    const options = ['land', 'ocean', 'air']
    for (var i = 1; i <= 3; i++) {
      records.push({
        name: options[i - 1],
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2),
        createdBy: 1,
        updatedBy: 1
      })
    }
    await queryInterface.bulkInsert('GearTypes', records, {})
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('GearTypes', null, {})
  }
}
