const faker = require('faker')
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const gearTypes = await queryInterface.sequelize.query(
      `SELECT id from GearTypes;`
    )
    const gearTypeRows = gearTypes[0]
    const records = []
    for (var i = 1; i <= 3; i++) {
      records.push({
        name: faker.commerce.productMaterial(),
        GearTypeId: gearTypeRows[faker.random.number({ min: 1, max: gearTypeRows.length })].id,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2),
        createdBy: 1,
        updatedBy: 1
      })
    }
    await queryInterface.bulkInsert('Gears', records, {})
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('Gears', null, {})
  }
}
