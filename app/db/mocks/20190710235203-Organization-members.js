'use strict'
const faker = require('faker')
module.exports = {
  up: (queryInterface, Sequelize) => {
    const OrganizationMembers = []
    for (var i = 1; i <= 3; i++) {
      OrganizationMembers.push({
        OrganizationRoleId: 1,
        OrganizationId: 1,
        UserId: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: faker.date.recent(2),
        updatedAt: faker.date.recent(2)
      })
    }
    return queryInterface.bulkInsert('OrganizationMembers', OrganizationMembers, {})
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('OrganizationMembers')
  }
}
