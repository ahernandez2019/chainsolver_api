// Models
const models = require('./models')

const checkConnection = async function () {
  try {
    await models.sequelize.authenticate()
    console.log('Database connection has been established successfully.')
  } catch (err) {
    throw err
  }
}

const sync = async function (force = true) {
  try {
    await models.sequelize.sync({ force })
    return 'Database schema synched successfully.'
  } catch (err) {
    console.error(err)
    throw err
  }
}

module.exports = {
  checkConnection,
  sync
}
