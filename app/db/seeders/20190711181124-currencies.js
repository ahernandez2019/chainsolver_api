'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "Currencies" ("id", "name", "label","symbol") VALUES
    (1, 'US Dollar', 'USD', '$');
    ALTER SEQUENCE "ContainerTypes" RESTART WITH 2
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Currencies', null, {})
  }
}
