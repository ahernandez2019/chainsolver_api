'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "Users" ("id", "name", "isVerified", "email", "password", "OrganizationId") VALUES
    (1, 'Carlos Ramirez', true, 'director@pic-cargo.com', 'PIC-cargo-2019+', 1);
    ALTER SEQUENCE "ContainerTypes" RESTART WITH 2
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ContainerTypes', null, {})
  }
}
