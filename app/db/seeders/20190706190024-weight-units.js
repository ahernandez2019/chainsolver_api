'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "WeightUnits" ("id", "name", "createdAt","updatedAt") VALUES
    (1, 'lb', '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z'),  
    (2, 'kg',  '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z'),
    (3, 'g',  '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z')
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WeightUnits', null, {})
  }
}
