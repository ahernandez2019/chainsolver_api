'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "ContainerTypes" ("id", "size", "type") VALUES
    (1, '20', 'ST'),  
    (2, '40', 'ST'),
    (3, '40', 'HQ'),  
    (4, '40', NOR');
    ALTER SEQUENCE "ContainerTypes" RESTART WITH 5
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ContainerTypes', null, {})
  }
}
