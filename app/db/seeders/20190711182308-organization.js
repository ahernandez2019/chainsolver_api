'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "Organizations" ("id", "name", "website", "OrganizationTypeId", "createdBy", "updatedBy", "createdAt", "updatedAt") VALUES
    (1, 'Personalized International Cargo', 'pic-cargo.com', 3, 1, 1, ${Date.now()},${Date.now()});
    ALTER SEQUENCE "ContainerTypes" RESTART WITH 2
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Organizations', null, {})
  }
}
