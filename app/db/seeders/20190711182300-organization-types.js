'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "OrganizationTypes" ("id", "name", "createdBy", "updatedBy") VALUES
    (1, 'Airline', 1, 1),
    (2, 'Shipping Company', 1, 1),
    (3, 'Agent', 1, 1),
    (4, 'Coloader', 1, 1),
    (5, 'Custom Agent', 1, 1),
    (6, 'Port', 1, 1),
    (7, 'Warehouse', 1, 1),
    (8, 'Carrier', 1, 1);
    ALTER SEQUENCE "ContainerTypes" RESTART WITH 9
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('OrganizationTypes', null, {})
  }
}
