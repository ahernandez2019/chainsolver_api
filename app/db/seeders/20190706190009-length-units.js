'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`INSERT INTO "LengthUnits" ("id", "name", "createdAt","updatedAt") VALUES
    (1, 'cm', '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z'),  
    (2, 'in', '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z'),
    (3, 'ft', '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z'),  
    (4, 'mm', '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z'),
    (5, 'm', '2018-07-20T14:57:56.000Z', '2018-07-20T14:57:56.000Z');
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('LengthUnits', null, {})
  }
}
