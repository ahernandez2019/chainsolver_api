
const client = require('../redis')

const checkToken = async function (req, res, next) {
  try {
    const token = req.headers.authorization.split(' ')[1]
    await client.get(token, function (err, reply) {
      if (err) {
        console.error(err)
      }
      console.log({ reply })
      if (reply) {
        res.status(500).json({ message: 'token is blacklisted' })
      }
    })
    next()
  } catch (e) {
    res.status(500).json({ e })
    console.error(e)
  }
}

module.exports = checkToken
