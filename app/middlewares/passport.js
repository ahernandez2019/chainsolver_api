const Models = require('../db/models')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const LocalStrategy = require('passport-local')

const localOptions = { usernameField: 'email' }

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Authorization'),
  secretOrKey: process.env.JWT_SECRET
}

const localLogin = new LocalStrategy(localOptions, async function (email, password, done) {
  try {
    const user = await Models.User.findOne({
      where: {
        email
      }
    })
    if (!user) {
      return done(null, false, {
        error: 'Your login details could not be verified. Please try again.' })
    } else if (user) {
      user.comparePassword(password, (err, isMatch) => {
        if (isMatch) {
          return done(null, user.dataValues)
        } else if (!isMatch) {
          return done(null, false, {
            error: 'Your login details could not be verified. Please try again.' })
        } else {
          done(err)
        }
      })
    }
  } catch (e) {
    return done(e)
  }
})

const jwtLogin = new JwtStrategy(jwtOptions, async function (payload, done) {
  try {
    const user = await Models.User.findByPk(payload.id)
    if (user) {
      return done(null, user)
    } else {
      return done(null, false)
    }
  } catch (e) {
    return done(e, false)
  }
})

module.exports = {
  jwtLogin,
  localLogin
}
