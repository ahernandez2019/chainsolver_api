
const winston = require('winston')

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
})

if (process.env.ENVIRONMENT === 'PRODUCTION') {
  logger.debug('Logging initialized at debug level')
}

module.exports = logger
