'use strict';

// Libraries
const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const jwtLogin = require('./middlewares/passport').jwtLogin
const localLogin = require('./middlewares/passport').localLogin
const morgan = require('morgan')
const passport = require('passport')
const Sentry = require('@sentry/node')
const helmet = require('helmet')
const rateLimit = require('express-rate-limit')
const compression = require('compression')

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('../swagger/chainsolver.json')

const env = require('../env')

if (process.env.ENVIRONMENT === 'PRODUCTION') {
  Sentry.init({ dsn: process.env.SENTRY_DSN })
}

// Router
const appRouter = require('./router')

const PORT = env.port

const onError = error => {
  if (error.syscall !== 'listen') {
    throw error
  }

  var bind = typeof port === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
    default:
      throw error
  }
}

const app = express()

app.use('/api-docs', swaggerUi.serve)
app.get('/api-docs', swaggerUi.setup(swaggerDocument))

app.use(helmet())

app.use(
  rateLimit({
    max: Number(process.env.RATE_LIMIT || 100),
    windowMs: 15 * 60 * 1000
  })
)
app.use(compression())
if (process.env.ENVIRONMENT === 'PRODUCTION') {
  app.use(Sentry.Handlers.requestHandler())
}
if (process.env.ENVIRONMENT === 'PRODUCTION') {
  app.use(Sentry.Handlers.errorHandler())
}
app.use(morgan('tiny'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// cors options
const corsOptions = {
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204
}
app.use(cors(corsOptions))

app.use('/api/v1', appRouter)

app.get('/', (req, res) => res.send({ message: 'Chainsolver working!!!' }))

passport.use(jwtLogin)
passport.use(localLogin)

app.on('error', onError)

module.exports = app

// multer
// auth0
