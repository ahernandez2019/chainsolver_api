// Env vars
require('dotenv').config();

const dev = {
  port: process.env.PORT || 8888,
  host: process.env.HOST || '0.0.0.0',
  db: {
    dbUri: process.env.DB_URI,
    dbPort: process.env.DB_PORT,
    dbHost: process.env.DB_HOST,
    db: process.env.DB_DATABASE,
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD
  }
};
const stagging = {
  port: process.env.PORT,
  host: process.env.HOST,
  db: {
    dbUri: process.env.DATABASE_URL,
    dbPort: process.env.DB_PORT,
    dbHost: process.env.DB_HOST,
    db: process.env.DB_DATABASE,
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD
  },
  sentry: {
    dsn: process.env.SENTRY_DSN
  }
};

const production = {
  ...stagging
};
// Constants
const env = {};
if (process.env.ENVIRONMENT === 'DEVELOPMENT') {
  env.variables = dev;
} else if (process.env.ENVIRONMENT === 'PRODUCTION') {
  env.variables = production;
} else if (process.env.ENVIRONMENT === 'STAGING') {
  env.variables = stagging;
}

module.exports = env.variables;
