# Chainsolver api

:truck:  A cargo and transportation management system built on top of Node.js, Express, Mongoose, Heroku, Redis, Nodemon, PM2, and Websockets.


This repository provides the following features:

* ---------- **Essentials** ----------
* [x] Application routing with [**Express**](http://expressjs.com/).
* [x] Object relational mapping with [**Sequelize**](http://docs.sequelizejs.com/).
* [x] Utility functions with [**Lodash**](https://lodash.com/).
* [] Reactive extensions with [**ReactiveX**](http://reactivex.io/).
* [x] Authenticate requests with [**Passport**](http://passportjs.org/).
* [] Real-time bidirectional communication with [**Socket.IO**](https://socket.io/).
* [x] In-memory data structure store with [**Redis**](https://redis.io/).
* [] Multi-protocol messaging broker with [**RabbitMQ**](https://www.rabbitmq.com/).
* [x] OpenAPI specification with [**Swagger**](https://swagger.io/).
* ---------- **Tools** ----------
* [x] Javascript style enforcing with [**Standard**](https://github.com/standard/standard).
* [x] JavaScript static code analyzer with [**ESLint**](https://github.com/eslint/eslint).
* [] Unit testing with [**Jest**](https://github.com/facebook/jest).
* [] End-to-End testing with [**Supertest**](https://github.com/visionmedia/supertest).
* [] Test coverage integration with [**Codecov**](https://codecov.io/).
* [x] Automatically restart application with [**Nodemon**](https://github.com/remy/nodemon).
* [x] Keeping application alive with [**PM2**](https://github.com/Unitech/pm2).
* [x] Git hooks git [**Husky**](https://github.com/typicode/husky).
* ---------- **Environments** ----------
* [x] JavaScript runtime with [**Node.js**](https://nodejs.org/).
* [x] Version control with [**Git**](https://github.com/git/git).
* [x] Code repository with [**bitbucket**](https://bitbucket.com/).
* [x] Cloud application hosting with [**Heroku**](https://www.heroku.com/).
* [x] Cloud SQL database hosting with  [**heroku postgres addon**](https://devcenter.heroku.com/articles/heroku-postgresql).
* [x] Cloud memory cache hosting with [**Rediscloud**](https://devcenter.heroku.com/articles/rediscloud).
* [x] distributed storage‎ hosting with [**Storj**](https://elements.heroku.com/addons/storj).
* [x] Cloud message queue hosting with [**CloudAMQP**](https://www.cloudamqp.com/).
* [x] Log management service with [**papertrail**](https://elements.heroku.com/addons/papertrail).
* [x] Error tracking service with [**Sentry**](https://sentry.io/).
* [x] Software container with [**Docker**](https://github.com/docker/docker).
* [x] Continuous integration with [**CodeshipCI**](https://elements.heroku.com/addons/codeship).

## Table of Contents

* [Getting Started](#getting-started)
* [Dockerization](#dockerization)
* [Configuration](#configuration)
* [Using Features](#using-features)
* [Creating Features](#creating-features)
* [Directory Structure](#directory-structure)
* [To do list](#to-do-list)

## Getting Started

Follow steps to execute the api.

1. Clone the repository

```bash
$ git clone --depth 1 clone https://jeanc20rlos@bitbucket.org/piccargo/chainsolver.git <PROJECT_NAME>
$ cd <PROJECT_NAME>
```

2. Build and run the container

```bash
$ npm run dev:up
```

3. In another terminal/tab run db:build to migrate, seed and create mock data

```bash
$ npm run db:build
```

3. Check code quality

```bash
$ npm run lint
```

6. Runs unit tests

```bash
$ npm run unit
```

7. Runs end-to-end tests

```bash
$ npm run e2e
```
## Dockerization

Dockerize an application.

1. Build and run the container in the background

```bash
$ npm run dev:up
```

2. Build db

```bash
$ npm run dev:build
```

3. Remove the old container before creating the new one

```bash
$ npm run dev:rm
```

4. Restart up the container and build

```bash
$ npm run dev:up--build
```

5. Stop the container

```bash
$ npm run dev:down
```
6. delete all dangling images and containers

```bash
$ npm run docker:clean
```

7. Restart up the container and build

```bash
$ npm run dev:up--build
```
8. Delete all images, containers and networks

```bash
$ npm run docker:prune
```
8. Build Docker Image

```bash
$ npm run docker:build
```

10. Push images to Docker Cloud

```diff
# .gitignore

  .DS_Store
  node_modules
  coverage
+ dev.Dockerfile
+ stage.Dockerfile
+ prod.Dockerfile
  *.log
```

```bash
$ docker login
$ docker build -f ./tools/<dev|stage|prod>.Dockerfile -t <IMAGE_NAME>:<IMAGE_TAG> .

# checkout
$ docker images

$ docker tag <IMAGE_NAME>:<IMAGE_TAG> <DOCKER_ID_USER>/<IMAGE_NAME>:<IMAGE_TAG>
$ docker push <DOCKER_ID_USER>/<IMAGE_NAME>:<IMAGE_TAG>

# remove
$ docker rmi <REPOSITORY>:<TAG>
# or
$ docker rmi <IMAGE_ID>
```

11. Pull images from Docker Cloud

```diff
# circle.yml

  echo "${HEROKU_TOKEN}" | docker login -u "${HEROKU_USERNAME}" --password-stdin registry.heroku.com
- docker build -f ./tools/$DEPLOYMENT_ENVIRONMENT.Dockerfile -t $APP_NAME .
+ docker pull <DOCKER_ID_USER>/<IMAGE_NAME>:<IMAGE_TAG>
- docker tag $APP_NAME registry.heroku.com/$APP_NAME/web
+ docker tag <IMAGE_NAME>:<IMAGE_TAG> registry.heroku.com/<HEROKU_PROJECT>/web
  docker push registry.heroku.com/<HEROKU_PROJECT>/web
```

## Configuration

### Default environments

Set your local environment variables.

```js
// src/env.js

export const dev = {
  port: process.env.PORT || 8080,
  host: process.env.HOST || '0.0.0.0',
  db: {
    dbUri: process.env.DB_URI,
    dbPort: process.env.DB_PORT,
    dbHost: process.env.DB_HOST,
    db: process.env.DB_DATABASE,
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD
  },
  redis: {
    redisUri: process.env.REDIS_URI,
    redisPort: process.env.REDIS_PORT,
    redisHost: process.env.REDIS_HOST,
    redisPassword: process.env.REDIS_PASSWORD
  }}

// ...
```
## Using Features

In order to use Restful powered features, you need to use the Swagger tools to test each endpoint and get some examples.



1. You can open [localhost/Swagger-ui](http://0.0.0.0:8081/) and take a look.

2. You can edit the local chain-solver json api specification to add your own feature, using the swagger-edditor, just oppening [localhost/Swagger-editor](http://0.0.0.0:8082/)

## Creating Features

If your want to create your own Restful Feature, you need to follow these steps

1. Create a new route in ``/app/router/``
```js
// Controllers
const myController = require('../controllers/myController')

// Router
const myRouter = express.Router()

myRouter.get('/', myController.getAll)
myRouter.get('/:id', myController.getById)
myRouter.post('/', myController.create)
myRouter.patch('/:id', myController.update)
myRouter.delete('/:id', myController.destroy)

module.exports = myRouter
```

2. Create a new Controller in ``/app/controllers/``

```js
const Models = require('../models')
const _ = require('lodash')

async function getAll (req, res) {
  try {
    const findParams = {
      limit: _.get(req, 'query.limit', 25),
      offset: _.get(req, 'query.offset', 0)
    }
    const myResource = await Models.myResource.findAll(findParams)
    res.status(200).send({ data: myResource })
  } catch (error) {
    console.error(error)
    const message = error.toString()
    res.status(500).send({ message })
  }
}

module.exports = {
  getAll
}

```

3. Create a route handler for your routes in ``/app/router/index,js``

```js
const express = require('express')
const router = express.Router({ strict: true })

router.use('/myRoute', require('./myRoute'))

module.exports = router

```

4. Create a model in ``/app/db/``

```js
const myModel = (sequelize, DataTypes) => {
  const MyModel = sequelize.define('MyModel', {
    field: {
      type: DataTypes.STRING,
      required: true
    },
  })
  }

  return MyModel
}

module.exports = myModel
```

5.  Open a new terminal, create a new migration in ``/app/db/migrations`` using 
```bash
$ sequelize migration:generate --name MyName
```
6.  Open a new terminal, create a new seed in ``/app/db/seed`` using 
```bash
$ sequelize seed:generate --name MyName
```
7.  Open a new terminal, create a new mock data seed in ``/app/db/migrations`` using 
```bash
$ sequelize seed:generate --name MyName --seeders-path app/db/mocks
```

6. Create a middleware

```js
const myMiddleware = async function (req, res, next) {
  try {
    
    await someData()
    })
    next()
  } catch (e) {
    res.status(500).json({ e })
    console.error(e)
  }
}

module.exports = myMiddleware

```

7. Example of Socket.io

```js
connSocket.emit('A', { foo: 'bar' });
connSocket.on('B', data => console.log(data));  // { foo: 'baz' }
```

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script>
const socket = io('http://localhost:3000/');

socket.on('connect', () => console.log('Socket: Accept a connection.'));

socket.on('A', (data) => {
  console.log(data);  // { foo: 'bar' }
  socket.emit('B', { foo: 'baz' });
});
</script>
```

8. Example of Redis

```js
const client = require('./redis');

redis.hmset('thing', {
  foo: 'js',
  bar: 'html',
  baz: 'css',
});

redis.hgetall('thing', (err, object) => {
  console.log(object);
});
```

## Directory Structure


```coffee
.
├── app 
│   ├── controllers -> feature controllers
│   │   ├──includes
│   │   │   └── ...
│   │   └──utils
│   │       └── ...
│   │
│   ├── db
│   │   ├──config -> db config files
│   │   ├──migrations -> db migration files
│   │   ├──mocks -> fake data seeder files
│   │   └──models -> db models
│   │
│   ├── middlewares -> middleware handlers
│   │   └── ...
│   │
│   ├── redis  -> redis function files
│   │   └── ...
│   │
│   ├── router  -> route files
│   │   └── ...
│   │
│   └── index.js
│   
├── swagger -> swagger json config
│   └── ...
│   
├── .dockerignore
├── .editorconfig
├── .eslintrc.js
├── .gitignore
├── .sequelizerc
├── docker-compose.yml
├── Dockerfile
├── env.js
├── package-lock.json
├── package.json
├── LICENSE
├── README.md
├── circle.yml
├── jest.config.js
├── server.js
└── processes.js
```
## To do list
* [] Setting up TDD using jest and supertest.
* [] Type the code (typescript/flow...).
* [] Use AuthO auth provider instead of passport.
* [] Set Up code Coverage.
* [] Set up Circle Ci.
* [] Implement a file manager feature.
* [] Implement shields feature.


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
