// Libraries
const app = require('./app')
const chalk = require('chalk')
const env = require('./env')
const PORT = env.port
const HOST = env.host

const start = async function () {
  try {
    app.listen(PORT, HOST, () => {
      console.log(chalk.hex('#009688')(' [*] App: Bootstrap Succeeded.'))
      console.log(chalk.hex('#009688')(` [*] Host: http://${HOST}:${PORT}/.`))
    })
  } catch (err) {
    console.log('ERROR', err)
  }
}

start()
